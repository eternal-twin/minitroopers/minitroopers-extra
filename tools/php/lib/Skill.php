<?php

use \php\Boot;
use \php\_Boot\HxEnum;

class Skill extends HxEnum {
    
	/**
     * @return Skill
     */
	public static function __callStatic($name, $args)
    {
        switch($name){
			case 'SOLDAT'             : return Skill::__CU000();
			case 'MEDIC'              : return Skill::__CU001();
			case 'PILOT'              : return Skill::__CU002();
			case 'ARTIFICER'          : return Skill::__CU003();
			case 'SCOUT'              : return Skill::__CU004();
			case 'SPY'                : return Skill::__CU005();
			case '\x1fj\x180\x01'     : return Skill::__CU006();
			case 'SABOTEUR'           : return Skill::__CU007();
			case 'PISTOL'             : return Skill::__CU008();
			case 'REVOLVER'           : return Skill::__CU009();
			case ',\x0eq\x13\x03'     : return Skill::__CU010();
			case ',!\x03q\x01'        : return Skill::__CU011();
			case '\x13\x07Sc\x02'     : return Skill::__CU012();
			case 'SHOTGUN'            : return Skill::__CU013();
			case 'J9I8\x02'           : return Skill::__CU014();
			case 'fm~\x01\x02'        : return Skill::__CU015();
			case '\x18\tZX\x03'       : return Skill::__CU016();
			case '4\x05v\x1a\x01'     : return Skill::__CU017();
			case '\x03\x1cz-\x03'     : return Skill::__CU018();
			case '\x05\x15\x07\x01'   : return Skill::__CU019();
			case 'e\x140\t\x01'       : return Skill::__CU020();
			case 'f\x10\x02;\x01'     : return Skill::__CU021();
			case '\nJ&\x01'        : return Skill::__CU022();
			case '\x14\x0e{\x01'      : return Skill::__CU023();
			case 'BAZOOKA_M1'         : return Skill::__CU024();
			case ';8TL\x02'           : return Skill::__CU025();
			case '*5x[\x02'           : return Skill::__CU026();
			case '\x1a\x17tQ'         : return Skill::__CU027();
			case 'COMANCHE_AUTO'      : return Skill::__CU028();
			case 'MACHINE_GUN'        : return Skill::__CU029();
			case 'GATLING_GUN'        : return Skill::__CU030();
			case 'MINIGUN'            : return Skill::__CU031();
			case 'X\x1fU'          : return Skill::__CU032();
			case 'yLzf\x02'           : return Skill::__CU033();
			case ' \x1b\b7'           : return Skill::__CU034();
			case 'ns3e'               : return Skill::__CU035();
			case '\x18\x0f; \x02'     : return Skill::__CU036();
			case 'hp\x01A'            : return Skill::__CU037();
			case '#wr_\x03'           : return Skill::__CU038();
			case 'Q$\\\x15\x02'       : return Skill::__CU039();
			case '\x134j\x10\x02'     : return Skill::__CU040();
			case 'g\x1fn\x18\x03'     : return Skill::__CU041();
			case 'K\'\x05i\x01'       : return Skill::__CU042();
			case 'b\x10z5\x02'        : return Skill::__CU043();
			case '\x1a,gm\x01'        : return Skill::__CU044();
			case '\x1fsZ4\x01'        : return Skill::__CU045();
			case 'iq#~\x01'           : return Skill::__CU046();
			case '5 11'               : return Skill::__CU047();
			case 'i77\'\x03'          : return Skill::__CU048();
			case '\'j\'A\x03'         : return Skill::__CU049();
			case '\x17tL_\x03'        : return Skill::__CU050();
			case '``9]\x03'           : return Skill::__CU051();
			case 'N&S'                : return Skill::__CU052();
			case '\x19(l;\x01'        : return Skill::__CU053();
			case '\x02I\x1aK'         : return Skill::__CU054();
			case '~\x13ds'            : return Skill::__CU055();
			case ';H\x1fu\x01'        : return Skill::__CU056();
			case '\x16\rz\x01\x02'    : return Skill::__CU057();
			case '10;\x07'            : return Skill::__CU058();
			case '#g\'T\x03'          : return Skill::__CU059();
			case '2\x04\x04/\x03'     : return Skill::__CU060();
			case 'q,\x04;\x01'        : return Skill::__CU061();
			case 'z\x1c\x1c\'\x01'    : return Skill::__CU062();
			case '\x1eMUn'            : return Skill::__CU063();
			case '\x13\x12\x147'      : return Skill::__CU064();
			case '@@jC'               : return Skill::__CU065();
			case '\b\\U#\x02'         : return Skill::__CU066();
			case 'h\x1fPw'            : return Skill::__CU067();
			case '50\b\x1a'           : return Skill::__CU068();
			case '\x1e4()\x02'        : return Skill::__CU069();
			case '(\x1f0`'            : return Skill::__CU070();
			case '\rswe\x03'          : return Skill::__CU071();
			case 'D\x04\x13\x11\x03'  : return Skill::__CU072();
			case 'i;j;'               : return Skill::__CU073();
			case '+,v\x1e\x01'        : return Skill::__CU074();
			case 'xtSb\x03'           : return Skill::__CU075();
			case 'AGJ\b\x03'          : return Skill::__CU076();
			case 'Y\x067 \x01'        : return Skill::__CU077();
			case '\x1f^0\x0b\x01'     : return Skill::__CU078();
			case ']{1\x1a\x03'        : return Skill::__CU079();
			case '\x1f6((\x02'        : return Skill::__CU080();
			case '&4y\f'              : return Skill::__CU081();
			case '\x03n\x16x\x03'     : return Skill::__CU082();
			case 'Wd\x017'            : return Skill::__CU083();
			case 'g\f\x0b\x01\x02'    : return Skill::__CU084();
			case '\x1b\x1fDW\x02'     : return Skill::__CU085();
			case '\x01\x13\x14C'      : return Skill::__CU086();
			case ';\x10I\x01'         : return Skill::__CU087();
			case '\x0bA\x1d(\x02'     : return Skill::__CU088();
			case 'C\t2\x06'           : return Skill::__CU089();
			case '\x02hD\x10\x01'     : return Skill::__CU090();
			case '+++;\x02'           : return Skill::__CU091();
			case 'v$\x07#'            : return Skill::__CU092();
			case '\x03cp1\x02'        : return Skill::__CU093();
			case '\x06\x12%N\x02'     : return Skill::__CU094();
			case 'J_R,'               : return Skill::__CU095();
			case 'o\x01Z%\x01'        : return Skill::__CU096();
			case '@K\x06\x1c\x01'     : return Skill::__CU097();
			case '1Y%\'\x02'          : return Skill::__CU098();
			case 'N\x19\x0fU\x02'     : return Skill::__CU099();
			case '1vDS\x02'           : return Skill::__CU100();
			case '9\x02\x14-\x03'     : return Skill::__CU101();
			case ';B=u\x02'           : return Skill::__CU102();
			case 'h$qD'               : return Skill::__CU103();
			case 'iU1O'               : return Skill::__CU104();
			case 'o%Ua\x02'           : return Skill::__CU105();
			case 'Z\x17LR\x03'        : return Skill::__CU106();
			case 'K\n\x18)'           : return Skill::__CU107();
			case '\x05n*'          : return Skill::__CU108();
			case ');ed\x01'           : return Skill::__CU109();
			case '_9?2\x01'           : return Skill::__CU110();
			case 'c&^X'               : return Skill::__CU111();
			case '3\tU;\x03'          : return Skill::__CU112();
			case 'Bxa\f\x02'          : return Skill::__CU113();
			case '8;\x0b\x0f'         : return Skill::__CU114();
			case '\x13HU\n\x02'       : return Skill::__CU115();
			case '\x17r$i\x03'        : return Skill::__CU116();
			case '\x10\nP\x1f\x01'    : return Skill::__CU117();
			case 'y+\x1d+\x02'        : return Skill::__CU118();
			case 'Qa%9\x02'           : return Skill::__CU119();
			case 'QnS\x07\x01'        : return Skill::__CU120();
			case '!\x14S\t\x01'       : return Skill::__CU121();
			case '\x1f\f t\x01'       : return Skill::__CU122();
			case '(~K['               : return Skill::__CU123();
			case 't`BP\x02'           : return Skill::__CU124();
			case '$__,\x01'           : return Skill::__CU125();
			case '\x07$8G\x02'        : return Skill::__CU126();
			case '0i1='               : return Skill::__CU127();
			case '\x15\x05\x19(\x02'  : return Skill::__CU128();
			case '\x01\x07qj'         : return Skill::__CU129();
			case '=STS'               : return Skill::__CU130();
			case '}\x10!)\x01'        : return Skill::__CU131();
			case 'ygnz\x03'           : return Skill::__CU132();
			case '*\x135r\x03'        : return Skill::__CU133();
			case ' *gn\x03'           : return Skill::__CU134();
			case '\x017CT\x02'        : return Skill::__CU135();
			case 'li9)\x01'           : return Skill::__CU136();
			case '\x18DJL'            : return Skill::__CU137();
			case ',\x02\bS\x01'       : return Skill::__CU138();
			case '*Xbz'               : return Skill::__CU139();
			case 'x\x18k\x01'      : return Skill::__CU140();
		}
    }
	
    /**
     * @return Skill
     */
    static public function __CU000 () { return new Skill('SOLDAT'            ,   0, []); }
    static public function __CU001 () { return new Skill('MEDIC'             ,   1, []); }
    static public function __CU002 () { return new Skill('PILOT'             ,   2, []); }
    static public function __CU003 () { return new Skill('ARTIFICER'         ,   3, []); }
    static public function __CU004 () { return new Skill('SCOUT'             ,   4, []); }
    static public function __CU005 () { return new Skill('SPY'               ,   5, []); }
    static public function __CU006 () { return new Skill('\x1fj\x180\x01'    ,   6, []); }
    static public function __CU007 () { return new Skill('SABOTEUR'          ,   7, []); }
    static public function __CU008 () { return new Skill('PISTOL'            ,   8, []); }
    static public function __CU009 () { return new Skill('REVOLVER'          ,   9, []); }
    static public function __CU010 () { return new Skill(',\x0eq\x13\x03'    ,  10, []); }
    static public function __CU011 () { return new Skill(',!\x03q\x01'       ,  11, []); }
    static public function __CU012 () { return new Skill('\x13\x07Sc\x02'    ,  12, []); }
    static public function __CU013 () { return new Skill('SHOTGUN'           ,  13, []); }
    static public function __CU014 () { return new Skill('J9I8\x02'          ,  14, []); }
    static public function __CU015 () { return new Skill('fm~\x01\x02'       ,  15, []); }
    static public function __CU016 () { return new Skill('\x18\tZX\x03'      ,  16, []); }
    static public function __CU017 () { return new Skill('4\x05v\x1a\x01'    ,  17, []); }
    static public function __CU018 () { return new Skill('\x03\x1cz-\x03'    ,  18, []); }
    static public function __CU019 () { return new Skill('\x05\x15\x07\x01'  ,  19, []); }
    static public function __CU020 () { return new Skill('e\x140\t\x01'      ,  20, []); }
    static public function __CU021 () { return new Skill('f\x10\x02;\x01'    ,  21, []); }
    static public function __CU022 () { return new Skill('\nJ&\x01'       ,  22, []); }
    static public function __CU023 () { return new Skill('\x14\x0e{\x01'     ,  23, []); }
    static public function __CU024 () { return new Skill('BAZOOKA_M1'        ,  24, []); }
    static public function __CU025 () { return new Skill(';8TL\x02'          ,  25, []); }
    static public function __CU026 () { return new Skill('*5x[\x02'          ,  26, []); }
    static public function __CU027 () { return new Skill('\x1a\x17tQ'        ,  27, []); }
    static public function __CU028 () { return new Skill('COMANCHE_AUTO'     ,  28, []); }
    static public function __CU029 () { return new Skill('MACHINE_GUN'       ,  29, []); }
    static public function __CU030 () { return new Skill('GATLING_GUN'       ,  30, []); }
    static public function __CU031 () { return new Skill('MINIGUN'           ,  31, []); }
    static public function __CU032 () { return new Skill('X\x1fU'         ,  32, []); }
    static public function __CU033 () { return new Skill('yLzf\x02'          ,  33, []); }
    static public function __CU034 () { return new Skill(' \x1b\b7'          ,  34, []); }
    static public function __CU035 () { return new Skill('ns3e'              ,  35, []); }
    static public function __CU036 () { return new Skill('\x18\x0f; \x02'    ,  36, []); }
    static public function __CU037 () { return new Skill('hp\x01A'           ,  37, []); }
    static public function __CU038 () { return new Skill('#wr_\x03'          ,  38, []); }
    static public function __CU039 () { return new Skill('Q$\\\x15\x02'      ,  39, []); }
    static public function __CU040 () { return new Skill('\x134j\x10\x02'    ,  40, []); }
    static public function __CU041 () { return new Skill('g\x1fn\x18\x03'    ,  41, []); }
    static public function __CU042 () { return new Skill('K\'\x05i\x01'      ,  42, []); }
    static public function __CU043 () { return new Skill('b\x10z5\x02'       ,  43, []); }
    static public function __CU044 () { return new Skill('\x1a,gm\x01'       ,  44, []); }
    static public function __CU045 () { return new Skill('\x1fsZ4\x01'       ,  45, []); }
    static public function __CU046 () { return new Skill('iq#~\x01'          ,  46, []); }
    static public function __CU047 () { return new Skill('5 11'              ,  47, []); }
    static public function __CU048 () { return new Skill('i77\'\x03'         ,  48, []); }
    static public function __CU049 () { return new Skill('\'j\'A\x03'        ,  49, []); }
    static public function __CU050 () { return new Skill('\x17tL_\x03'       ,  50, []); }
    static public function __CU051 () { return new Skill('``9]\x03'          ,  51, []); }
    static public function __CU052 () { return new Skill('N&S'               ,  52, []); }
    static public function __CU053 () { return new Skill('\x19(l;\x01'       ,  53, []); }
    static public function __CU054 () { return new Skill('\x02I\x1aK'        ,  54, []); }
    static public function __CU055 () { return new Skill('~\x13ds'           ,  55, []); }
    static public function __CU056 () { return new Skill(';H\x1fu\x01'       ,  56, []); }
    static public function __CU057 () { return new Skill('\x16\rz\x01\x02'   ,  57, []); }
    static public function __CU058 () { return new Skill('10;\x07'           ,  58, []); }
    static public function __CU059 () { return new Skill('#g\'T\x03'         ,  59, []); }
    static public function __CU060 () { return new Skill('2\x04\x04/\x03'    ,  60, []); }
    static public function __CU061 () { return new Skill('q,\x04;\x01'       ,  61, []); }
    static public function __CU062 () { return new Skill('z\x1c\x1c\'\x01'   ,  62, []); }
    static public function __CU063 () { return new Skill('\x1eMUn'           ,  63, []); }
    static public function __CU064 () { return new Skill('\x13\x12\x147'     ,  64, []); }
    static public function __CU065 () { return new Skill('@@jC'              ,  65, []); }
    static public function __CU066 () { return new Skill('\b\\U#\x02'        ,  66, []); }
    static public function __CU067 () { return new Skill('h\x1fPw'           ,  67, []); }
    static public function __CU068 () { return new Skill('50\b\x1a'          ,  68, []); }
    static public function __CU069 () { return new Skill('\x1e4()\x02'       ,  69, []); }
    static public function __CU070 () { return new Skill('(\x1f0`'           ,  70, []); }
    static public function __CU071 () { return new Skill('\rswe\x03'         ,  71, []); }
    static public function __CU072 () { return new Skill('D\x04\x13\x11\x03' ,  72, []); }
    static public function __CU073 () { return new Skill('i;j;'              ,  73, []); }
    static public function __CU074 () { return new Skill('+,v\x1e\x01'       ,  74, []); }
    static public function __CU075 () { return new Skill('xtSb\x03'          ,  75, []); }
    static public function __CU076 () { return new Skill('AGJ\b\x03'         ,  76, []); }
    static public function __CU077 () { return new Skill('Y\x067 \x01'       ,  77, []); }
    static public function __CU078 () { return new Skill('\x1f^0\x0b\x01'    ,  78, []); }
    static public function __CU079 () { return new Skill(']{1\x1a\x03'       ,  79, []); }
    static public function __CU080 () { return new Skill('\x1f6((\x02'       ,  80, []); }
    static public function __CU081 () { return new Skill('&4y\f'             ,  81, []); }
    static public function __CU082 () { return new Skill('\x03n\x16x\x03'    ,  82, []); }
    static public function __CU083 () { return new Skill('Wd\x017'           ,  83, []); }
    static public function __CU084 () { return new Skill('g\f\x0b\x01\x02'   ,  84, []); }
    static public function __CU085 () { return new Skill('\x1b\x1fDW\x02'    ,  85, []); }
    static public function __CU086 () { return new Skill('\x01\x13\x14C'     ,  86, []); }
    static public function __CU087 () { return new Skill(';\x10I\x01'        ,  87, []); }
    static public function __CU088 () { return new Skill('\x0bA\x1d(\x02'    ,  88, []); }
    static public function __CU089 () { return new Skill('C\t2\x06'          ,  89, []); }
    static public function __CU090 () { return new Skill('\x02hD\x10\x01'    ,  90, []); }
    static public function __CU091 () { return new Skill('+++;\x02'          ,  91, []); }
    static public function __CU092 () { return new Skill('v$\x07#'           ,  92, []); }
    static public function __CU093 () { return new Skill('\x03cp1\x02'       ,  93, []); }
    static public function __CU094 () { return new Skill('\x06\x12%N\x02'    ,  94, []); }
    static public function __CU095 () { return new Skill('J_R,'              ,  95, []); }
    static public function __CU096 () { return new Skill('o\x01Z%\x01'       ,  96, []); }
    static public function __CU097 () { return new Skill('@K\x06\x1c\x01'    ,  97, []); }
    static public function __CU098 () { return new Skill('1Y%\'\x02'         ,  98, []); }
    static public function __CU099 () { return new Skill('N\x19\x0fU\x02'    ,  99, []); }
    static public function __CU100 () { return new Skill('1vDS\x02'          , 100, []); }
    static public function __CU101 () { return new Skill('9\x02\x14-\x03'    , 101, []); }
    static public function __CU102 () { return new Skill(';B=u\x02'          , 102, []); }
    static public function __CU103 () { return new Skill('h$qD'              , 103, []); }
    static public function __CU104 () { return new Skill('iU1O'              , 104, []); }
    static public function __CU105 () { return new Skill('o%Ua\x02'          , 105, []); }
    static public function __CU106 () { return new Skill('Z\x17LR\x03'       , 106, []); }
    static public function __CU107 () { return new Skill('K\n\x18)'          , 107, []); }
    static public function __CU108 () { return new Skill('\x05n*'         , 108, []); }
    static public function __CU109 () { return new Skill(');ed\x01'          , 109, []); }
    static public function __CU110 () { return new Skill('_9?2\x01'          , 110, []); }
    static public function __CU111 () { return new Skill('c&^X'              , 111, []); }
    static public function __CU112 () { return new Skill('3\tU;\x03'         , 112, []); }
    static public function __CU113 () { return new Skill('Bxa\f\x02'         , 113, []); }
    static public function __CU114 () { return new Skill('8;\x0b\x0f'        , 114, []); }
    static public function __CU115 () { return new Skill('\x13HU\n\x02'      , 115, []); }
    static public function __CU116 () { return new Skill('\x17r$i\x03'       , 116, []); }
    static public function __CU117 () { return new Skill('\x10\nP\x1f\x01'   , 117, []); }
    static public function __CU118 () { return new Skill('y+\x1d+\x02'       , 118, []); }
    static public function __CU119 () { return new Skill('Qa%9\x02'          , 119, []); }
    static public function __CU120 () { return new Skill('QnS\x07\x01'       , 120, []); }
    static public function __CU121 () { return new Skill('!\x14S\t\x01'      , 121, []); }
    static public function __CU122 () { return new Skill('\x1f\f t\x01'      , 122, []); }
    static public function __CU123 () { return new Skill('(~K['              , 123, []); }
    static public function __CU124 () { return new Skill('t`BP\x02'          , 124, []); }
    static public function __CU125 () { return new Skill('$__,\x01'          , 125, []); }
    static public function __CU126 () { return new Skill('\x07$8G\x02'       , 126, []); }
    static public function __CU127 () { return new Skill('0i1='              , 127, []); }
    static public function __CU128 () { return new Skill('\x15\x05\x19(\x02' , 128, []); }
    static public function __CU129 () { return new Skill('\x01\x07qj'        , 129, []); }
    static public function __CU130 () { return new Skill('=STS'              , 130, []); }
    static public function __CU131 () { return new Skill('}\x10!)\x01'       , 131, []); }
    static public function __CU132 () { return new Skill('ygnz\x03'          , 132, []); }
    static public function __CU133 () { return new Skill('*\x135r\x03'       , 133, []); }
    static public function __CU134 () { return new Skill(' *gn\x03'          , 134, []); }
    static public function __CU135 () { return new Skill('\x017CT\x02'       , 135, []); }
    static public function __CU136 () { return new Skill('li9)\x01'          , 136, []); }
    static public function __CU137 () { return new Skill('\x18DJL'           , 137, []); }
    static public function __CU138 () { return new Skill(',\x02\bS\x01'      , 138, []); }
    static public function __CU139 () { return new Skill('*Xbz'              , 139, []); }
    static public function __CU140 () { return new Skill('x\x18k\x01'     , 140, []); }


    /**
     * Returns array of (constructorIndex => constructorName)
     *
     * @return string[]
     */
    static public function __hx__list () {
        return [
              0 =>  'SOLDAT'            ,
              1 =>  'MEDIC'             ,
              2 =>  'PILOT'             ,
              3 =>  'ARTIFICER'         ,
              4 =>  'SCOUT'             ,
              5 =>  'SPY'               ,
              6 => '\x1fj\x180\x01'    ,
              7 => 'SABOTEUR'          ,
              8 => 'PISTOL'            ,
              9 => 'REVOLVER'          ,
             10 => ',\x0eq\x13\x03'    ,
             11 => ',!\x03q\x01'       ,
             12 => '\x13\x07Sc\x02'    ,
             13 =>  'SHOTGUN'           ,
             14 => 'J9I8\x02'          ,
             15 => 'fm~\x01\x02'       ,
             16 => '\x18\tZX\x03'      ,
             17 => '4\x05v\x1a\x01'    ,
             18 => '\x03\x1cz-\x03'    ,
             19 => '\x05\x15\x07\x01'  ,
             20 => 'e\x140\t\x01'      ,
             21 => 'f\x10\x02;\x01'    ,
             22 => '\nJ&\x01'       ,
             23 => '\x14\x0e{\x01'     ,
             24 =>  'BAZOOKA_M1'        ,
             25 => ';8TL\x02'          ,
             26 => '*5x[\x02'          ,
             27 => '\x1a\x17tQ'        ,
             28 =>  'COMANCHE_AUTO'     ,
             29 =>  'MACHINE_GUN'       ,
             30 =>  'GATLING_GUN'       ,
             31 =>  'MINIGUN'           ,
             32 => 'X\x1fU'         ,
             33 => 'yLzf\x02'          ,
             34 => ' \x1b\b7'          ,
             35 => 'ns3e'              ,
             36 => '\x18\x0f; \x02'    ,
             37 => 'hp\x01A'           ,
             38 => '#wr_\x03'          ,
             39 => 'Q$\\\x15\x02'      ,
             40 => '\x134j\x10\x02'    ,
             41 => 'g\x1fn\x18\x03'    ,
             42 => 'K\'\x05i\x01'      ,
             43 => 'b\x10z5\x02'       ,
             44 => '\x1a,gm\x01'       ,
             45 => '\x1fsZ4\x01'       ,
             46 => 'iq#~\x01'          ,
             47 => '5 11'              ,
             48 => 'i77\'\x03'         ,
             49 => '\'j\'A\x03'        ,
             50 => '\x17tL_\x03'       ,
             51 => '``9]\x03'          ,
             52 => 'N&S'               ,
             53 => '\x19(l;\x01'       ,
             54 => '\x02I\x1aK'        ,
             55 => '~\x13ds'           ,
             56 => ';H\x1fu\x01'       ,
             57 => '\x16\rz\x01\x02'   ,
             58 => '10;\x07'           ,
             59 => '#g\'T\x03'         ,
             60 => '2\x04\x04/\x03'    ,
             61 => 'q,\x04;\x01'       ,
             62 => 'z\x1c\x1c\'\x01'   ,
             63 => '\x1eMUn'           ,
             64 => '\x13\x12\x147'     ,
             65 => '@@jC'              ,
             66 => '\b\\U#\x02'        ,
             67 => 'h\x1fPw'           ,
             68 => '50\b\x1a'          ,
             69 => '\x1e4()\x02'       ,
             70 => '(\x1f0`'           ,
             71 => '\rswe\x03'         ,
             72 => 'D\x04\x13\x11\x03' ,
             73 => 'i;j;'              ,
             74 => '+,v\x1e\x01'       ,
             75 => 'xtSb\x03'          ,
             76 => 'AGJ\b\x03'         ,
             77 => 'Y\x067 \x01'       ,
             78 => '\x1f^0\x0b\x01'    ,
             79 => ']{1\x1a\x03'       ,
             80 => '\x1f6((\x02'       ,
             81 => '&4y\f'             ,
             82 => '\x03n\x16x\x03'    ,
             83 => 'Wd\x017'           ,
             84 => 'g\f\x0b\x01\x02'   ,
             85 => '\x1b\x1fDW\x02'    ,
             86 => '\x01\x13\x14C'     ,
             87 => ';\x10I\x01'        ,
             88 => '\x0bA\x1d(\x02'    ,
             89 => 'C\t2\x06'          ,
             90 => '\x02hD\x10\x01'    ,
             91 => '+++;\x02'          ,
             92 => 'v$\x07#'           ,
             93 => '\x03cp1\x02'       ,
             94 => '\x06\x12%N\x02'    ,
             95 => 'J_R,'              ,
             96 => 'o\x01Z%\x01'       ,
             97 => '@K\x06\x1c\x01'    ,
             98 => '1Y%\'\x02'         ,
             99 => 'N\x19\x0fU\x02'    ,
            100 => '1vDS\x02'          ,
            101 => '9\x02\x14-\x03'    ,
            102 => ';B=u\x02'          ,
            103 => 'h$qD'              ,
            104 => 'iU1O'              ,
            105 => 'o%Ua\x02'          ,
            106 => 'Z\x17LR\x03'       ,
            107 => 'K\n\x18)'          ,
            108 => '\x05n*'         ,
            109 => ');ed\x01'          ,
            110 => '_9?2\x01'          ,
            111 => 'c&^X'              ,
            112 => '3\tU;\x03'         ,
            113 => 'Bxa\f\x02'         ,
            114 => '8;\x0b\x0f'        ,
            115 => '\x13HU\n\x02'      ,
            116 => '\x17r$i\x03'       ,
            117 => '\x10\nP\x1f\x01'   ,
            118 => 'y+\x1d+\x02'       ,
            119 => 'Qa%9\x02'          ,
            120 => 'QnS\x07\x01'       ,
            121 => '!\x14S\t\x01'      ,
            122 => '\x1f\f t\x01'      ,
            123 => '(~K['              ,
            124 => 't`BP\x02'          ,
            125 => '$__,\x01'          ,
            126 => '\x07$8G\x02'       ,
            127 => '0i1='              ,
            128 => '\x15\x05\x19(\x02' ,
            129 => '\x01\x07qj'        ,
            130 => '=STS'              ,
            131 => '}\x10!)\x01'       ,
            132 => 'ygnz\x03'          ,
            133 => '*\x135r\x03'       ,
            134 => ' *gn\x03'          ,
            135 => '\x017CT\x02'       ,
            136 => 'li9)\x01'          ,
            137 => '\x18DJL'           ,
            138 => ',\x02\bS\x01'      ,
            139 => '*Xbz'              ,
            140 => 'x\x18k\x01'     ,
        ];
    }

    /**
     * Returns array of (constructorName => parametersCount)
     *
     * @return int[]
     */
    static public function __hx__paramsCount () {
        return [
            'SOLDAT'               => 0,
            'MEDIC'                => 0,
            'PILOT'                => 0,
            'ARTIFICER'            => 0,
            'SCOUT'                => 0,
            'SPY'                  => 0,
            '\x1fj\x180\x01'     => 0,
            'SABOTEUR'             => 0,
            'PISTOL'               => 0,
            'REVOLVER'             => 0,
            ',\x0eq\x13\x03'     => 0,
            ',!\x03q\x01'        => 0,
            '\x13\x07Sc\x02'     => 0,
            'SHOTGUN'              => 0,
            'J9I8\x02'           => 0,
            'fm~\x01\x02'        => 0,
            '\x18\tZX\x03'       => 0,
            '4\x05v\x1a\x01'     => 0,
            '\x03\x1cz-\x03'     => 0,
            '\x05\x15\x07\x01'   => 0,
            'e\x140\t\x01'       => 0,
            'f\x10\x02;\x01'     => 0,
            '\nJ&\x01'        => 0,
            '\x14\x0e{\x01'      => 0,
            'BAZOOKA_M1'           => 0,
            ';8TL\x02'           => 0,
            '*5x[\x02'           => 0,
            '\x1a\x17tQ'         => 0,
            'COMANCHE_AUTO'        => 0,
            'MACHINE_GUN'          => 0,
            'GATLING_GUN'          => 0,
            'MINIGUN'              => 0,
            'X\x1fU'          => 0,
            'yLzf\x02'           => 0,
            ' \x1b\b7'           => 0,
            'ns3e'               => 0,
            '\x18\x0f; \x02'     => 0,
            'hp\x01A'            => 0,
            '#wr_\x03'           => 0,
            'Q$\\\x15\x02'       => 0,
            '\x134j\x10\x02'     => 0,
            'g\x1fn\x18\x03'     => 0,
            'K\'\x05i\x01'       => 0,
            'b\x10z5\x02'        => 0,
            '\x1a,gm\x01'        => 0,
            '\x1fsZ4\x01'        => 0,
            'iq#~\x01'           => 0,
            '5 11'               => 0,
            'i77\'\x03'          => 0,
            '\'j\'A\x03'         => 0,
            '\x17tL_\x03'        => 0,
            '``9]\x03'           => 0,
            'N&S'                => 0,
            '\x19(l;\x01'        => 0,
            '\x02I\x1aK'         => 0,
            '~\x13ds'            => 0,
            ';H\x1fu\x01'        => 0,
            '\x16\rz\x01\x02'    => 0,
            '10;\x07'            => 0,
            '#g\'T\x03'          => 0,
            '2\x04\x04/\x03'     => 0,
            'q,\x04;\x01'        => 0,
            'z\x1c\x1c\'\x01'    => 0,
            '\x1eMUn'            => 0,
            '\x13\x12\x147'      => 0,
            '@@jC'               => 0,
            '\b\\U#\x02'         => 0,
            'h\x1fPw'            => 0,
            '50\b\x1a'           => 0,
            '\x1e4()\x02'        => 0,
            '(\x1f0`'            => 0,
            '\rswe\x03'          => 0,
            'D\x04\x13\x11\x03'  => 0,
            'i;j;'               => 0,
            '+,v\x1e\x01'        => 0,
            'xtSb\x03'           => 0,
            'AGJ\b\x03'          => 0,
            'Y\x067 \x01'        => 0,
            '\x1f^0\x0b\x01'     => 0,
            ']{1\x1a\x03'        => 0,
            '\x1f6((\x02'        => 0,
            '&4y\f'              => 0,
            '\x03n\x16x\x03'     => 0,
            'Wd\x017'            => 0,
            'g\f\x0b\x01\x02'    => 0,
            '\x1b\x1fDW\x02'     => 0,
            '\x01\x13\x14C'      => 0,
            ';\x10I\x01'         => 0,
            '\x0bA\x1d(\x02'     => 0,
            'C\t2\x06'           => 0,
            '\x02hD\x10\x01'     => 0,
            '+++;\x02'           => 0,
            'v$\x07#'            => 0,
            '\x03cp1\x02'        => 0,
            '\x06\x12%N\x02'     => 0,
            'J_R,'               => 0,
            'o\x01Z%\x01'        => 0,
            '@K\x06\x1c\x01'     => 0,
            '1Y%\'\x02'          => 0,
            'N\x19\x0fU\x02'     => 0,
            '1vDS\x02'           => 0,
            '9\x02\x14-\x03'     => 0,
            ';B=u\x02'           => 0,
            'h$qD'               => 0,
            'iU1O'               => 0,
            'o%Ua\x02'           => 0,
            'Z\x17LR\x03'        => 0,
            'K\n\x18)'           => 0,
            '\x05n*'          => 0,
            ');ed\x01'           => 0,
            '_9?2\x01'           => 0,
            'c&^X'               => 0,
            '3\tU;\x03'          => 0,
            'Bxa\f\x02'          => 0,
            '8;\x0b\x0f'         => 0,
            '\x13HU\n\x02'       => 0,
            '\x17r$i\x03'        => 0,
            '\x10\nP\x1f\x01'    => 0,
            'y+\x1d+\x02'        => 0,
            'Qa%9\x02'           => 0,
            'QnS\x07\x01'        => 0,
            '!\x14S\t\x01'       => 0,
            '\x1f\f t\x01'       => 0,
            '(~K['               => 0,
            't`BP\x02'           => 0,
            '$__,\x01'           => 0,
            '\x07$8G\x02'        => 0,
            '0i1='               => 0,
            '\x15\x05\x19(\x02'  => 0,
            '\x01\x07qj'         => 0,
            '=STS'               => 0,
            '}\x10!)\x01'        => 0,
            'ygnz\x03'           => 0,
            '*\x135r\x03'        => 0,
            ' *gn\x03'           => 0,
            '\x017CT\x02'        => 0,
            'li9)\x01'           => 0,
            '\x18DJL'            => 0,
            ',\x02\bS\x01'       => 0,
            '*Xbz'               => 0,
            'x\x18k\x01'      => 0,
        ];
    }
}

Boot::registerClass(Skill::class, 'Skill');