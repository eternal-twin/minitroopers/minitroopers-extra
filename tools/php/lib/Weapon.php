<?php

use \php\Boot;
use \php\_Boot\HxEnum;

class Weapon extends HxEnum {
    
	/**
     * @return Weapon
     */
	public static function __callStatic($name, $args)
    {
        switch($name){
			case 'ud\\\\'          : return Weapon::__CU00 ();
			case 'Ug/O\x01'        : return Weapon::__CU01 ();
			case 'Z\tvt\x01'       : return Weapon::__CU02 ();
			case '\x0bK\x1cW\x03'  : return Weapon::__CU03 ();
			case ')(g|'            : return Weapon::__CU04 ();
			case 'd\'s0\x01'       : return Weapon::__CU05 ();
			case 'nVL\x1e'         : return Weapon::__CU06 ();
			case 'Xp?\x16\x03'     : return Weapon::__CU07 ();
			case '(7*\\\x01'       : return Weapon::__CU08 ();
			case '\th/i'           : return Weapon::__CU09 ();
			case '\x17}&d\x01'     : return Weapon::__CU10 ();
			case '5^Lv\x01'        : return Weapon::__CU11 ();
			case ')L0t\x02'        : return Weapon::__CU12 ();
			case 'zT5p\x02'        : return Weapon::__CU13 ();
			case 'je\x03^'         : return Weapon::__CU14 ();
			case '`XGm\x02'        : return Weapon::__CU15 ();
			case 'K\\]n\x03'       : return Weapon::__CU16 ();
			case '#i6\x1b'         : return Weapon::__CU17 ();
			case 'AvZZ\x03'        : return Weapon::__CU18 ();
			case '\x06\x0f\x1b\f'  : return Weapon::__CU19 ();
			case '|y}L\x03'        : return Weapon::__CU20 ();
			case '\x04(Kd\x03'     : return Weapon::__CU21 ();
			case 'SG\tY\x03'       : return Weapon::__CU22 ();
			case 'L!Ak'            : return Weapon::__CU23 ();
			case '\'z\x06\x10\x03' : return Weapon::__CU24 ();
			case '\x1c~t%\x01'     : return Weapon::__CU25 ();
			case 'v*E2\x03'        : return Weapon::__CU26 ();
			case 'o\x12Q\x07\x03'  : return Weapon::__CU27 ();
			case 'I\x17;\x12\x03'  : return Weapon::__CU28 ();
			case '||e\x07\x03'     : return Weapon::__CU29 ();
			case 'F\x06w)'         : return Weapon::__CU30 ();
			case '$sD \x02'        : return Weapon::__CU31 ();
			case '+W+H\x02'        : return Weapon::__CU32 ();
			case '8\x13\x1cJ\x03'  : return Weapon::__CU33 ();
			case 'qf~O\x01'        : return Weapon::__CU34 ();
			case '\x05t\ra\x03'    : return Weapon::__CU35 ();
			case '1[a@\x01'        : return Weapon::__CU36 ();
			case '\x1c\x1dS,\x03'  : return Weapon::__CU37 ();
		}
    }
	
    /**
     * @return Weapon
     */
    static public function __CU00 () { return new Weapon('ud\\\\'         ,  0, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU01 () { return new Weapon('Ug/O\x01'       ,  1, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU02 () { return new Weapon('Z\tvt\x01'      ,  2, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU03 () { return new Weapon('\x0bK\x1cW\x03' ,  3, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU04 () { return new Weapon(')(g|'           ,  4, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU05 () { return new Weapon('d\'s0\x01'      ,  5, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU06 () { return new Weapon('nVL\x1e'        ,  6, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU07 () { return new Weapon('Xp?\x16\x03'    ,  7, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU08 () { return new Weapon('(7*\\\x01'      ,  8, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU09 () { return new Weapon('\th/i'          ,  9, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU10 () { return new Weapon('\x17}&d\x01'    , 10, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU11 () { return new Weapon('5^Lv\x01'       , 11, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU12 () { return new Weapon(')L0t\x02'       , 12, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU13 () { return new Weapon('zT5p\x02'       , 13, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU14 () { return new Weapon('je\x03^'        , 14, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU15 () { return new Weapon('`XGm\x02'       , 15, []); }
	
    /**
     * @return Weapon
     */
	
    /**
     * @return Weapon
     */
    static public function __CU16 () { return new Weapon('K\\]n\x03'      , 16, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU17 () { return new Weapon('#i6\x1b'        , 17, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU18 () { return new Weapon('AvZZ\x03'       , 18, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU19 () { return new Weapon('\x06\x0f\x1b\f' , 19, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU20 () { return new Weapon('|y}L\x03'       , 20, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU21 () { return new Weapon('\x04(Kd\x03'    , 21, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU22 () { return new Weapon('SG\tY\x03'      , 22, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU23 () { return new Weapon('L!Ak'           , 23, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU24 () { return new Weapon('\'z\x06\x10\x03', 24, []); }
	
    /**
     * @return Weapon
     */
	
    /**
     * @return Weapon
     */
    static public function __CU25 () { return new Weapon('\x1c~t%\x01'    , 25, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU26 () { return new Weapon('v*E2\x03'       , 26, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU27 () { return new Weapon('o\x12Q\x07\x03' , 27, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU28 () { return new Weapon('I\x17;\x12\x03' , 28, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU29 () { return new Weapon('||e\x07\x03'    , 29, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU30 () { return new Weapon('F\x06w)'        , 30, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU31 () { return new Weapon('$sD \x02'       , 31, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU32 () { return new Weapon('+W+H\x02'       , 32, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU33 () { return new Weapon('8\x13\x1cJ\x03' , 33, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU34 () { return new Weapon('qf~O\x01'       , 34, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU35 () { return new Weapon('\x05t\ra\x03'   , 35, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU36 () { return new Weapon('1[a@\x01'       , 36, []); }
	
    /**
     * @return Weapon
     */
    static public function __CU37 () { return new Weapon('\x1c\x1dS,\x03' , 37, []); }


    /**
     * Returns array of (constructorIndex => constructorName)
     *
     * @return string[]
     */
    static public function __hx__list () {
        return [
             0 => 'ud\\\\'         ,
             1 => 'Ug/O\x01'       ,
             2 => 'Z\tvt\x01'      ,
             3 => '\x0bK\x1cW\x03' ,
             4 => ')(g|'           ,
             5 => 'd\'s0\x01'      ,
             6 => 'nVL\x1e'        ,
             7 => 'Xp?\x16\x03'    ,
             8 => '(7*\\\x01'      ,
             9 => '\th/i'          ,
            10 => '\x17}&d\x01'    ,
            11 => '5^Lv\x01'       ,
            12 => ')L0t\x02'       ,
            13 => 'zT5p\x02'       ,
            14 => 'je\x03^'        ,
            15 => '`XGm\x02'       ,
            16 => 'K\\]n\x03'      ,
            17 => '#i6\x1b'        ,
            18 => 'AvZZ\x03'       ,
            19 => '\x06\x0f\x1b\f' ,
            20 => '|y}L\x03'       ,
            21 => '\x04(Kd\x03'    ,
            22 => 'SG\tY\x03'      ,
            23 => 'L!Ak'           ,
            24 => '\'z\x06\x10\x03',
            25 => '\x1c~t%\x01'    ,
            26 => 'v*E2\x03'       ,
            27 => 'o\x12Q\x07\x03' ,
            28 => 'I\x17;\x12\x03' ,
            29 => '||e\x07\x03'    ,
            30 => 'F\x06w)'        ,
            31 => '$sD \x02'       ,
            32 => '+W+H\x02'       ,
            33 => '8\x13\x1cJ\x03' ,
            34 => 'qf~O\x01'       ,
            35 => '\x05t\ra\x03'   ,
            36 => '1[a@\x01'       ,
            37 => '\x1c\x1dS,\x03' ,
        ];
    }

    /**
     * Returns array of (constructorName => parametersCount)
     *
     * @return int[]
     */
    static public function __hx__paramsCount () {
        return [
            'ud\\\\'          => 0,
            'Ug/O\x01'        => 0,
            'Z\tvt\x01'       => 0,
            '\x0bK\x1cW\x03'  => 0,
            ')(g|'            => 0,
            'd\'s0\x01'       => 0,
            'nVL\x1e'         => 0,
            'Xp?\x16\x03'     => 0,
            '(7*\\\x01'       => 0,
            '\th/i'           => 0,
            '\x17}&d\x01'     => 0,
            '5^Lv\x01'        => 0,
            ')L0t\x02'        => 0,
            'zT5p\x02'        => 0,
            'je\x03^'         => 0,
            '`XGm\x02'        => 0,
            'K\\]n\x03'       => 0,
            '#i6\x1b'         => 0,
            'AvZZ\x03'        => 0,
            '\x06\x0f\x1b\f'  => 0,
            '|y}L\x03'        => 0,
            '\x04(Kd\x03'     => 0,
            'SG\tY\x03'       => 0,
            'L!Ak'            => 0,
            '\'z\x06\x10\x03' => 0,
            '\x1c~t%\x01'     => 0,
            'v*E2\x03'        => 0,
            'o\x12Q\x07\x03'  => 0,
            'I\x17;\x12\x03'  => 0,
            '||e\x07\x03'     => 0,
            'F\x06w)'         => 0,
            '$sD \x02'        => 0,
            '+W+H\x02'        => 0,
            '8\x13\x1cJ\x03'  => 0,
            'qf~O\x01'        => 0,
            '\x05t\ra\x03'    => 0,
            '1[a@\x01'        => 0,
            '\x1c\x1dS,\x03'  => 0,
        ];
    }
}

Boot::registerClass(Weapon::class, 'Weapon');