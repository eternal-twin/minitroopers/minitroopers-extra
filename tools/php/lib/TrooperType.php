<?php

use \php\Boot;
use \php\_Boot\HxEnum;

class TrooperType extends HxEnum {
	
    /**
     * @return TrooperType
     */
    static public function HUMAN  () { return new TrooperType("HUMAN",  0, []); }
	
    /**
     * @return TrooperType
     */
    static public function PUPPET () { return new TrooperType("PUPPET", 1, []); }
	
    /**
     * @return TrooperType
     */
    static public function RAT    () { return new TrooperType("RAT",    2, []); }

    /**
     * Returns array of (constructorIndex => constructorName)
     *
     * @return string[]
     */
    static public function __hx__list () {
        return [
            0 => "HUMAN", 
            1 => "PUPPET",
            2 => "RAT",   
        ];
    }

    /**
     * Returns array of (constructorName => parametersCount)
     *
     * @return int[]
     */
    static public function __hx__paramsCount () {
        return [
            "HUMAN"   => 0,
            "PUPPET"  => 0,
            "RAT"     => 0,
        ];
    }
}

Boot::registerClass(TrooperType::class, 'TrooperType');