<?php

use \php\Boot;
use \php\_Boot\HxEnum;

class ReloadSystem extends HxEnum {
    
	/**
     * @return ReloadSystem
     */
	public static function __callStatic($name, $args)
    {
        switch($name){
			case 'dI^r\x03'       : return ReloadSystem::__CU0();
			case '@&\b\x19\x01'   : return ReloadSystem::__CU1();
			case '\x15Z^\x1d\x01' : return ReloadSystem::__CU2();
			case 'ZI,C\x01'       : return ReloadSystem::__CU3();
		}
    }
	
    /**
     * @return ReloadSystem
     */
    static public function __CU0 () { return new ReloadSystem('dI^r\x03'      , 0, []); }
	
    /**
     * @return ReloadSystem
     */
    static public function __CU1 () { return new ReloadSystem('@&\b\x19\x01'  , 1, []); }
	
    /**
     * @return ReloadSystem
     */
    static public function __CU2 () { return new ReloadSystem('\x15Z^\x1d\x01', 2, []); }
	
    /**
     * @return ReloadSystem
     */
    static public function __CU3 () { return new ReloadSystem('ZI,C\x01'      , 3, []); }

    /**
     * Returns array of (constructorIndex => constructorName)
     *
     * @return string[]
     */
    static public function __hx__list () {
        return [
            0 => 'dI^r\x03'      ,
            1 => '@&\b\x19\x01'  ,
            2 => '\x15Z^\x1d\x01',
            3 => 'ZI,C\x01'      ,
        ];
    }

    /**
     * Returns array of (constructorName => parametersCount)
     *
     * @return int[]
     */
    static public function __hx__paramsCount () {
        return [
            'dI^r\x03'       => 0,
            '@&\b\x19\x01'   => 0,
            '\x15Z^\x1d\x01' => 0,
            'ZI,C\x01'       => 0,
        ];
    }
}

Boot::registerClass(ReloadSystem::class, 'ReloadSystem');