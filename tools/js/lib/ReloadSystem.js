class ReloadSystem extends Enum {
	static __construct__ = ["dI^r\x03", "@&\b\x19\x01", "\x15Z^\x1d\x01", "ZI,C\x01"];
	static 'dI^r\x03'       = new this("dI^r\x03",       0);
	static '@&\b\x19\x01'   = new this("@&\b\x19\x01",   1);
	static '\x15Z^\x1d\x01' = new this("\x15Z^\x1d\x01", 2);
	static 'ZI,C\x01'       = new this("ZI,C\x01",       3);
}
ReloadSystem.resolve();