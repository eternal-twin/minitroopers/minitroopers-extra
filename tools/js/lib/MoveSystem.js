class MoveSystem extends Enum {
	static __construct__ = ["\n\x01O\x02", "}C9g", "J\x16$2"];
	static '\n\x01O\x02' = new this("\n\x01O\x02", 0);
	static '}C9g'           = new this("}C9g",           1);
	static 'J\x16$2'        = new this("J\x16$2",        2);
}
MoveSystem.resolve();