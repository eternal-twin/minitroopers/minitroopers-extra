class TargetSystem extends Enum {
	static __construct__ = ["0QCw\x01", "rf_U\x03", "f`\x16\x02", "%i\x1e2\x01"];
	static '0QCw\x01'      = new this("0QCw\x01",      0);
	static 'rf_U\x03'      = new this("rf_U\x03",      1);
	static 'f`\x16\x02' = new this("f`\x16\x02", 2);
	static '%i\x1e2\x01' = new this("%i\x1e2\x01", 3);
}
TargetSystem.resolve();