class TargetType extends Enum {
	static __construct__ = ["3YX,\x03", "RO)%"];
	static '3YX,\x03' = new this('3YX,\x03', 0);
	/*
	 * @param _Skill<Enum> arg0 : ???
	 */
	static 'RO)%'(arg0) { return new this('RO)%', 0, arg0); }
}
TargetType.resolve();