function startBattle()
{
	if(titre != '')
	{
		$('swf_battle').style.backgroundColor = bgColor;
		$('swf_battle').innerHTML = "<table cellpadding = '0px' cellspacing = '0px' id = 'table-titre' style = 'margin : auto;'><tr><td style = 'text-align : center; overflow : hidden; width : 720px; height : 440px; font-size : 30px; font-weight : bold; color : #B89E87;'>" + titre + "</td></tr></table>";
		setTimeout("effacerTitre(0);", 1000);
	}
	else
		realStartBattle();
}

function realStartBattle()
{
	$('swf_battle').innerHTML = '<object data="http://data.minitroopers.fr/swf/client_fr.swf?v=2" class="swf" id="battle" type="application/x-shockwave-flash" height="440" width="720"><param value="false" name="menu"><param value="' + data + '" name="flashvars"><param value="noscale" name="scale"><param value="always" name="allowScriptAccess"><param value="' + bgColor + '" name="bgcolor"></object>';
}


function effacerTitre(nb)
{
	if(nb != 25)
	{
		node = $('table-titre');
		opa = (25 - parseInt(nb)) * 4;
		node.style.opacity = opa / 100;
		node.style.MozOpacity = opa / 100;
		node.style.KhtmlOpacity = opa / 100;
		node.style.filter = "alpha(opacity=" + opa + ")";
		setTimeout("effacerTitre(" + (parseInt(nb) + 1) + ");", 40)
	}
	else
		realStartBattle();
}

/*
     FILE ARCHIVED ON 11:55:26 Oct 20, 2011 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 12:17:21 Jul 26, 2022.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  captures_list: 96.736
  exclusion.robots: 0.319
  exclusion.robots.policy: 0.305
  RedisCDXSource: 0.82
  esindex: 0.015
  LoadShardBlock: 70.415 (3)
  PetaboxLoader3.datanode: 82.406 (4)
  CDXLines.iter: 20.347 (3)
  load_resource: 72.257
  PetaboxLoader3.resolve: 56.711
*/