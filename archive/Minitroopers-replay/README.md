# Minitroopers-replay

Site externe combiné à un userscript pour archiver et visionner les batailles.

### Auteur

iow - 2011~2016 (non enregistré sur Twinoid)
`http://iow.ludiserv.com/mreplay/` (expiré)

### Twinpedia

Le but de ce script est de stocker les vidéos de combat de vos troopers indéfiniment, car vous l'aurez remarqué, les liens normaux vers un combat ont une durée de vie très limitée.

> Lorsque vous aurez installé ce script, un nouveau bouton apparaitra sur la page lors de votre combat, “Sauver ce combat”.
> Si le combat vous plaît, cliquez sur ce bouton.
> Une fois sauvegardé, un nouveau bouton apparaitra, “Lien du replay”

Cliquez dessus et vous obtiendrez les liens pour partager ce combat indéfiniment!!
Vos vidéos

### Présentation initiale

> Bonjour à toutes et à tous,
> 
> étant moi-même passionné par ce petit jeu, je me suis souvent senti frustré de ne pas pouvoir partager mes meilleurs combats avec d'autres, faute de " durée de vie " des liens minitroopers. C'est pourquoi j'ai développé un petit outils permettant de sauver indéfiniment combats, raids, infiltrations, exterminations ou épiques ! Le développement n'est pas encore terminé totalement, mais j'pense que ça vous plaira !
> 
> Bref voila le lien
> `link=http://iow.ludiserv.com/mreplay/`
> 
> ET pour fêter ça un petit replay :
> 
> 4 VS 23 rats
> `link=http://iow.ludiserv.com/mreplay/replay-lx9g3vpiwagkdst8hc51.html`
> 
> En espérant que ca puisse servir à quelqu'un ^^'

[source](http://twd.io/e/th8O/0)
