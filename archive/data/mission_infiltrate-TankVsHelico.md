# Tank vs hélico

![Tank vs hélico](../screenshots/mission_infiltrate-TankVsHelico.png "Archive")

## Mission: Infiltrate

- Main: [L'armée d'147](http://147.minitroopers.fr/hq)
- Opp: [L'armée de vache-rose](http://vache-rose.minitroopers.fr/hq)

## flashvars

<details><summary>Object</summary>

```javascript
{
  "mode": [
    "eU\\u\u0001",
    0,
    {
      "bg": {
        "id": [
          "BG_SEWER",
          2
        ],
        "gfx": "bg/sewer.jpg"
      },
      "id": 21304808,
      "armies": [
        {
          "troopers": [
            {
              "name": "connard",
              "pref": {
                "moveSystem": [
                  "\n\u0001O\u0002",
                  0
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [],
                "__CBody": null,
                "__CWeapon": [
                  "#i6\u001b",
                  17
                ],
                "targetType": [
                  "RO)%",
                  0,
                  [
                    "SOLDAT",
                    0
                  ]
                ],
                "targetSystem": [
                  "rf_U\u0003",
                  1
                ]
              },
              "seed": 1194067,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                1,
                0,
                0,
                0,
                0,
                0,
                1
              ],
              "id": 11,
              "force": []
            },
            {
              "name": "tata",
              "pref": {
                "moveSystem": [
                  "\n\u0001O\u0002",
                  0
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [],
                "__CBody": [
                  "TORSO_LEFT",
                  1
                ],
                "__CWeapon": [
                  "v*E2\u0003",
                  26
                ],
                "targetType": [
                  "RO)%",
                  0,
                  [
                    "SABOTEUR",
                    7
                  ]
                ],
                "targetSystem": [
                  "%i\u001e2\u0001",
                  3
                ]
              },
              "seed": 7378584,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                0,
                1,
                0,
                1,
                0,
                1,
                0
              ],
              "id": 12,
              "force": []
            },
            {
              "name": "fiss de put",
              "pref": {
                "moveSystem": [
                  "J\u0016$2",
                  2
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [],
                "__CBody": [
                  "TORSO_LEFT",
                  1
                ],
                "__CWeapon": [
                  "v*E2\u0003",
                  26
                ],
                "targetType": [
                  "RO)%",
                  0,
                  [
                    "SOLDAT",
                    0
                  ]
                ],
                "targetSystem": [
                  "0QCw\u0001",
                  0
                ]
              },
              "seed": 11397232,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                0,
                1,
                0,
                1,
                0,
                0,
                0
              ],
              "id": 13,
              "force": []
            },
            {
              "name": "le pgm",
              "pref": {
                "moveSystem": [
                  "\n\u0001O\u0002",
                  0
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [],
                "__CBody": [
                  "HEAD",
                  0
                ],
                "__CWeapon": null,
                "targetType": [
                  "RO)%",
                  0,
                  [
                    "PILOT",
                    2
                  ]
                ],
                "targetSystem": [
                  "%i\u001e2\u0001",
                  3
                ]
              },
              "seed": 5571185,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                1,
                0,
                0,
                0,
                1,
                1,
                1
              ],
              "id": 14,
              "force": []
            },
            {
              "name": "anculler",
              "pref": {
                "moveSystem": [
                  "J\u0016$2",
                  2
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [],
                "__CBody": [
                  "HEAD",
                  0
                ],
                "__CWeapon": null,
                "targetType": [
                  "RO)%",
                  0,
                  [
                    "SOLDAT",
                    0
                  ]
                ],
                "targetSystem": [
                  "%i\u001e2\u0001",
                  3
                ]
              },
              "seed": 10333089,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                0,
                1,
                0,
                1,
                1,
                1,
                1,
                1
              ],
              "id": 15,
              "force": []
            }
          ],
          "__UP2": [
            11,
            12,
            13,
            14,
            15
          ],
          "faction": 4
        },
        {
          "troopers": [
            {
              "name": "Doc. G",
              "pref": {
                "moveSystem": [
                  "\n\u0001O\u0002",
                  0
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [
                  [
                    "Y\u00067 \u0001",
                    77
                  ],
                  [
                    "9\u0002\u0014-\u0003",
                    101
                  ],
                  [
                    "2\u0004\u0004/\u0003",
                    60
                  ]
                ],
                "__CBody": [
                  "STOMACH",
                  3
                ],
                "__CWeapon": [
                  "\th/i",
                  9
                ],
                "targetType": [
                  "RO)%",
                  0,
                  [
                    "SABOTEUR",
                    7
                  ]
                ],
                "targetSystem": [
                  "0QCw\u0001",
                  0
                ]
              },
              "seed": 8526740,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                1,
                0,
                1,
                1,
                1,
                1,
                0,
                1,
                1,
                0,
                0,
                0,
                1,
                0,
                1,
                1,
                1,
                0
              ],
              "id": 0,
              "force": []
            },
            {
              "name": "Kab'",
              "pref": {
                "moveSystem": [
                  "J\u0016$2",
                  2
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [
                  [
                    "\b\\U#\u0002",
                    66
                  ],
                  [
                    "2\u0004\u0004/\u0003",
                    60
                  ]
                ],
                "__CBody": [
                  "HEAD",
                  0
                ],
                "__CWeapon": [
                  "L!Ak",
                  23
                ],
                "targetType": [
                  "RO)%",
                  0,
                  [
                    "PILOT",
                    2
                  ]
                ],
                "targetSystem": [
                  "rf_U\u0003",
                  1
                ]
              },
              "seed": 3577799,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                0,
                1,
                1,
                0,
                0,
                0,
                1,
                1,
                0,
                1,
                0,
                1,
                0,
                1,
                1,
                0,
                0,
                1
              ],
              "id": 1,
              "force": []
            },
            {
              "name": "Alu",
              "pref": {
                "moveSystem": [
                  "}C9g",
                  1
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [
                  [
                    "1vDS\u0002",
                    100
                  ],
                  [
                    "Wd\u00017",
                    83
                  ],
                  [
                    ";H\u001fu\u0001",
                    56
                  ],
                  [
                    "\u0001\u0013\u0014C",
                    86
                  ],
                  [
                    "10;\u0007",
                    58
                  ],
                  [
                    "\u0002hD\u0010\u0001",
                    90
                  ]
                ],
                "__CBody": null,
                "__CWeapon": null,
                "targetType": [
                  "3YX,\u0003",
                  0
                ],
                "targetSystem": [
                  "0QCw\u0001",
                  0
                ]
              },
              "seed": 2241330,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                1,
                1,
                0,
                0,
                0,
                0,
                1,
                0,
                1,
                1,
                0,
                1,
                0,
                0,
                1,
                1,
                0,
                1,
                0
              ],
              "id": 2,
              "force": []
            },
            {
              "name": "PQ-WC",
              "pref": {
                "moveSystem": [
                  "\n\u0001O\u0002",
                  0
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [
                  [
                    "h$qD",
                    103
                  ],
                  [
                    "1vDS\u0002",
                    100
                  ]
                ],
                "__CBody": [
                  "TORSO_LEFT",
                  1
                ],
                "__CWeapon": [
                  "\u0004(Kd\u0003",
                  21
                ],
                "targetType": [
                  "RO)%",
                  0,
                  [
                    "SCOUT",
                    4
                  ]
                ],
                "targetSystem": [
                  "0QCw\u0001",
                  0
                ]
              },
              "seed": 1113839,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                0,
                0,
                0,
                0,
                1,
                0,
                1,
                0,
                0,
                1,
                1,
                0,
                0,
                1,
                1,
                1,
                0,
                1
              ],
              "id": 3,
              "force": []
            },
            {
              "name": "tedkak t'aur",
              "pref": {
                "moveSystem": [
                  "\n\u0001O\u0002",
                  0
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [
                  [
                    "\rswe\u0003",
                    71
                  ]
                ],
                "__CBody": [
                  "HEAD",
                  0
                ],
                "__CWeapon": [
                  "L!Ak",
                  23
                ],
                "targetType": [
                  "RO)%",
                  0,
                  [
                    "PILOT",
                    2
                  ]
                ],
                "targetSystem": [
                  "0QCw\u0001",
                  0
                ]
              },
              "seed": 3185039,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                0,
                1,
                0,
                1,
                0,
                1,
                1,
                1,
                0,
                0,
                1,
                0,
                1,
                0,
                1,
                1,
                1,
                0
              ],
              "id": 4,
              "force": []
            },
            {
              "name": "noob",
              "pref": {
                "moveSystem": [
                  "\n\u0001O\u0002",
                  0
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [
                  [
                    "$__,\u0001",
                    125
                  ]
                ],
                "__CBody": [
                  "TORSO_LEFT",
                  1
                ],
                "__CWeapon": [
                  "#i6\u001b",
                  17
                ],
                "targetType": [
                  "RO)%",
                  0,
                  [
                    "SABOTEUR",
                    7
                  ]
                ],
                "targetSystem": [
                  "%i\u001e2\u0001",
                  3
                ]
              },
              "seed": 10790622,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                1,
                0,
                1,
                1,
                0,
                0,
                1,
                0,
                0,
                0,
                1,
                0,
                0,
                0,
                0,
                1,
                0,
                0
              ],
              "id": 5,
              "force": []
            },
            {
              "name": "maxime",
              "pref": {
                "moveSystem": [
                  "}C9g",
                  1
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [
                  [
                    "iq#~\u0001",
                    46
                  ],
                  [
                    "10;\u0007",
                    58
                  ],
                  [
                    "Y\u00067 \u0001",
                    77
                  ],
                  [
                    "\u0016\rz\u0001\u0002",
                    57
                  ]
                ],
                "__CBody": [
                  "TORSO_LEFT",
                  1
                ],
                "__CWeapon": [
                  "AvZZ\u0003",
                  18
                ],
                "targetType": [
                  "RO)%",
                  0,
                  [
                    "ARTIFICER",
                    3
                  ]
                ],
                "targetSystem": [
                  "%i\u001e2\u0001",
                  3
                ]
              },
              "seed": 11828349,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                1,
                0,
                0,
                0,
                1,
                1,
                1,
                0,
                1,
                0,
                1,
                0,
                0,
                1,
                0,
                0,
                0,
                0
              ],
              "id": 6,
              "force": []
            },
            {
              "name": "lord olalaa",
              "pref": {
                "moveSystem": [
                  "J\u0016$2",
                  2
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [
                  [
                    ";H\u001fu\u0001",
                    56
                  ]
                ],
                "__CBody": [
                  "STOMACH",
                  3
                ],
                "__CWeapon": [
                  "v*E2\u0003",
                  26
                ],
                "targetType": [
                  "RO)%",
                  0,
                  [
                    "SOLDAT",
                    0
                  ]
                ],
                "targetSystem": [
                  "rf_U\u0003",
                  1
                ]
              },
              "seed": 7775586,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                0,
                0,
                0,
                1,
                0,
                1,
                0,
                1,
                0,
                0,
                0,
                1,
                1,
                1,
                1,
                1,
                0,
                0
              ],
              "id": 7,
              "force": []
            },
            {
              "name": "High-way",
              "pref": {
                "moveSystem": [
                  "}C9g",
                  1
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [
                  [
                    "C\t2\u0006",
                    89
                  ],
                  [
                    "\b\\U#\u0002",
                    66
                  ],
                  [
                    "iq#~\u0001",
                    46
                  ],
                  [
                    "Wd\u00017",
                    83
                  ]
                ],
                "__CBody": null,
                "__CWeapon": null,
                "targetType": [
                  "3YX,\u0003",
                  0
                ],
                "targetSystem": [
                  "0QCw\u0001",
                  0
                ]
              },
              "seed": 946287,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                1,
                1,
                1,
                1,
                0,
                0,
                1,
                0,
                0,
                0,
                0,
                1,
                0,
                2,
                1,
                0,
                2,
                2,
                1
              ],
              "id": 8,
              "force": []
            },
            {
              "name": "Caed\"",
              "pref": {
                "moveSystem": [
                  "}C9g",
                  1
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [
                  [
                    "10;\u0007",
                    58
                  ],
                  [
                    "iq#~\u0001",
                    46
                  ]
                ],
                "__CBody": null,
                "__CWeapon": null,
                "targetType": [
                  "3YX,\u0003",
                  0
                ],
                "targetSystem": [
                  "0QCw\u0001",
                  0
                ]
              },
              "seed": 11434433,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                1,
                1,
                1,
                0,
                0,
                1,
                1,
                0,
                1,
                1,
                1,
                1,
                1,
                0,
                1,
                0,
                0,
                1,
                1
              ],
              "id": 9,
              "force": []
            },
            {
              "name": "Québecois",
              "pref": {
                "moveSystem": [
                  "}C9g",
                  1
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [
                  [
                    "z\u001c\u001c'\u0001",
                    62
                  ],
                  [
                    "g\f\u000b\u0001\u0002",
                    84
                  ]
                ],
                "__CBody": [
                  "LEG_RIGHT",
                  7
                ],
                "__CWeapon": [
                  "\th/i",
                  9
                ],
                "targetType": [
                  "RO)%",
                  0,
                  [
                    "\u001fj\u00180\u0001",
                    6
                  ]
                ],
                "targetSystem": [
                  "f`\u0016\u0002",
                  2
                ]
              },
              "seed": 1977754,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                0,
                1,
                1,
                1,
                0,
                0,
                0,
                1,
                1,
                0,
                0,
                0,
                0,
                1,
                0,
                1,
                0,
                1
              ],
              "id": 10,
              "force": []
            }
          ],
          "__UP2": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10
          ],
          "faction": 2
        }
      ]
    },
    0
  ],
  "gfx": "http://data.minitroopers.fr/swf/army.swf?v=2"
}
```
</details>

<details><summary>Raw</summary>

`data=oy10%3A%253B%2501%250CZjy10%3AClientMode%3A0%3A2oy7%3A%252AM%2501oy5%3A7X%2501jy14%3ABackgroundType%3A2%3A0y8%3A%2524PS%2501y14%3Abg%252Fsewer.jpggR3i21304808y11%3Aa%255De%251B%2502aoy13%3A%25601%251F%252A%2502aoy8%3A%25032%251A4y7%3Aconnardy8%3AV%251BE%257Coy9%3A%251DlQK%2503jy10%3AMoveSystem%3A0%3A0y13%3A%2511%2521%250Ea%2501jy12%3AReloadSystem%3A2%3A0y11%3A%25290%2519i%2503ahy15%3A%251A%2529%2513%253D%2501ny13%3A%250BR%2510%251B%2502jy6%3AWeapon%3A17%3A0y13%3A%2515S%2517%253F%2503jy10%3ATargetType%3A1%3A1jy5%3ASkill%3A0%3A0y6%3A%253BuXEjy12%3ATargetSystem%3A1%3A0gy6%3A%250CA2ci1194067y6%3AAn%25256jy11%3ATrooperType%3A0%3A0y8%3A%250Az%250Avai1zzzzzi1hR3i11y13%3A%257D%2526%2516Y%2501ahgoR9y4%3AtataR11oR12jR13%3A0%3A0R14jR15%3A2%3A0R16ahR17jy7%3ABodyLoc%3A1%3A0R18jR19%3A26%3A0R20jR21%3A1%3A1jR22%3A7%3A0R23jR24%3A3%3A0gR25i7378584R26jR27%3A0%3A0R28azi1zi1zi1zhR3i12R29ahgoR9y15%3Afiss%2520de%2520putR11oR12jR13%3A2%3A0R14jR15%3A2%3A0R16ahR17jR31%3A1%3A0R18jR19%3A26%3A0R20jR21%3A1%3A1jR22%3A0%3A0R23jR24%3A0%3A0gR25i11397232R26jR27%3A0%3A0R28azi1zi1zzzhR3i13R29ahgoR9y8%3Ale%2520pgmR11oR12jR13%3A0%3A0R14jR15%3A2%3A0R16ahR17jR31%3A0%3A0R18nR20jR21%3A1%3A1jR22%3A2%3A0R23jR24%3A3%3A0gR25i5571185R26jR27%3A0%3A0R28ai1zzzi1i1i1hR3i14R29ahgoR9y8%3AancullerR11oR12jR13%3A2%3A0R14jR15%3A2%3A0R16ahR17jR31%3A0%3A0R18nR20jR21%3A1%3A1jR22%3A0%3A0R23jR24%3A3%3A0gR25i10333089R26jR27%3A0%3A0R28azi1zi1i1i1i1i1hR3i15R29ahghy13%3AK%2529%2516%250A%2502ai11i12i13i14i15hy9%3AOPz%2516%2503i4goR8aoR9y8%3ADoc.%2520GR11oR12jR13%3A0%3A0R14jR15%3A2%3A0R16ajR22%3A77%3A0jR22%3A101%3A0jR22%3A60%3A0hR17jR31%3A3%3A0R18jR19%3A9%3A0R20jR21%3A1%3A1jR22%3A7%3A0R23jR24%3A0%3A0gR25i8526740R26jR27%3A0%3A0R28ai1zi1i1i1i1zi1i1zzzi1zi1i1i1zhR3zR29ahgoR9y6%3AKab%2527R11oR12jR13%3A2%3A0R14jR15%3A2%3A0R16ajR22%3A66%3A0jR22%3A60%3A0hR17jR31%3A0%3A0R18jR19%3A23%3A0R20jR21%3A1%3A1jR22%3A2%3A0R23jR24%3A1%3A0gR25i3577799R26jR27%3A0%3A0R28azi1i1zzzi1i1zi1zi1zi1i1zzi1hR3i1R29ahgoR9y3%3AAluR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ajR22%3A100%3A0jR22%3A83%3A0jR22%3A56%3A0jR22%3A86%3A0jR22%3A58%3A0jR22%3A90%3A0hR17nR18nR20jR21%3A0%3A0R23jR24%3A0%3A0gR25i2241330R26jR27%3A0%3A0R28ai1i1zzzzi1zi1i1zi1zzi1i1zi1zhR3i2R29ahgoR9y5%3APQ-WCR11oR12jR13%3A0%3A0R14jR15%3A2%3A0R16ajR22%3A103%3A0jR22%3A100%3A0hR17jR31%3A1%3A0R18jR19%3A21%3A0R20jR21%3A1%3A1jR22%3A4%3A0R23jR24%3A0%3A0gR25i1113839R26jR27%3A0%3A0R28azzzzi1zi1zzi1i1zzi1i1i1zi1hR3i3R29ahgoR9y16%3Atedkak%2520t%2527aurR11oR12jR13%3A0%3A0R14jR15%3A2%3A0R16ajR22%3A71%3A0hR17jR31%3A0%3A0R18jR19%3A23%3A0R20jR21%3A1%3A1jR22%3A2%3A0R23jR24%3A0%3A0gR25i3185039R26jR27%3A0%3A0R28azi1zi1zi1i1i1zzi1zi1zi1i1i1zhR3i4R29ahgoR9y4%3AnoobR11oR12jR13%3A0%3A0R14jR15%3A2%3A0R16ajR22%3A125%3A0hR17jR31%3A1%3A0R18jR19%3A17%3A0R20jR21%3A1%3A1jR22%3A7%3A0R23jR24%3A3%3A0gR25i10790622R26jR27%3A0%3A0R28ai1zi1i1zzi1zzzi1zzzzi1zzhR3i5R29ahgoR9y6%3AmaximeR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ajR22%3A46%3A0jR22%3A58%3A0jR22%3A77%3A0jR22%3A57%3A0hR17jR31%3A1%3A0R18jR19%3A18%3A0R20jR21%3A1%3A1jR22%3A3%3A0R23jR24%3A3%3A0gR25i11828349R26jR27%3A0%3A0R28ai1zzzi1i1i1zi1zi1zzi1zzzzhR3i6R29ahgoR9y13%3Alord%2520olalaaR11oR12jR13%3A2%3A0R14jR15%3A2%3A0R16ajR22%3A56%3A0hR17jR31%3A3%3A0R18jR19%3A26%3A0R20jR21%3A1%3A1jR22%3A0%3A0R23jR24%3A1%3A0gR25i7775586R26jR27%3A0%3A0R28azzzi1zi1zi1zzzi1i1i1i1i1zzhR3i7R29ahgoR9y8%3AHigh-wayR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ajR22%3A89%3A0jR22%3A66%3A0jR22%3A46%3A0jR22%3A83%3A0hR17nR18nR20jR21%3A0%3A0R23jR24%3A0%3A0gR25i946287R26jR27%3A0%3A0R28ai1i1i1i1zzi1zzzzi1zi2i1zi2i2i1hR3i8R29ahgoR9y7%3ACaed%2522R11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ajR22%3A58%3A0jR22%3A46%3A0hR17nR18nR20jR21%3A0%3A0R23jR24%3A0%3A0gR25i11434433R26jR27%3A0%3A0R28ai1i1i1zzi1i1zi1i1i1i1i1zi1zzi1i1hR3i9R29ahgoR9y14%3AQu%25C3%25A9becoisR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ajR22%3A62%3A0jR22%3A84%3A0hR17jR31%3A7%3A0R18jR19%3A9%3A0R20jR21%3A1%3A1jR22%3A6%3A0R23jR24%3A2%3A0gR25i1977754R26jR27%3A0%3A0R28azi1i1i1zzzi1i1zzzzi1zi1zi1hR3i10R29ahghR35azi1i2i3i4i5i6i7i8i9i10hR36i2ghgzR5y58%3Ahttp%253A%252F%252Fdata.minitroopers.fr%252Fswf%252Farmy.swf%253Fv%253D2g&chk=0c0b5ee25c1401a4adce75b63b2634e4&imageurl=http://data.minitroopers.fr/img/&endMsg=Ce%20combat%20s%27est%20sold%C3%A9%20par%20un%20%C3%A9chec%20%21`
</details>