# Skills mini

Il s'agit des icones pouvant figurer dans les 3 slots à gauche de la fiche d'un trooper

![spritesheet](../gfx/skills-equip/spritesheet.png)

## Liste

icon | ID | skill | name
:-:|:-:|:-:|---
![icon 44](../gfx/skills-equip/44_piqure_d_amphet.png)       |  44 | ![skill 44](../gfx/skills/skill_44.png)   | piqure d'amphet
![icon 58](../gfx/skills-equip/58-balle_paralysante.png)     |  58 | ![skill 58](../gfx/skills/skill_58.png)   | balle paralysante
![icon 46](../gfx/skills-equip/46-sac_a_dos.png)             |  46 | ![skill 46](../gfx/skills/skill_46.png)   | sac à dos
![icon 57](../gfx/skills-equip/57-balle_hydrochock.png)      |  57 | ![skill 57](../gfx/skills/skill_57.png)   | balle hydrochock
![icon 56](../gfx/skills-equip/56-balle_explosive.png)       |  56 | ![skill 56](../gfx/skills/skill_56.png)   | balle explosive
![icon 60](../gfx/skills-equip/60-balle_toxique.png)         |  60 | ![skill 60](../gfx/skills/skill_60.png)   | balle toxique
![icon 62](../gfx/skills-equip/62-chargeur.png)              |  62 | ![skill 62](../gfx/skills/skill_62.png)   | chargeur
![icon 66](../gfx/skills-equip/66-termos_de_cafe.png)        |  66 | ![skill 66](../gfx/skills/skill_66.png)   | termos de café
![icon 77](../gfx/skills-equip/77-premiers_soins.png)        |  77 | ![skill 77](../gfx/skills/skill_77.png)   | premiers soins
![icon 81](../gfx/skills-equip/81-armure_complete.png)       |  81 | ![skill 81](../gfx/skills/skill_81.png)   | armure complete
![icon 83](../gfx/skills-equip/83-grenade_flash.png)         |  83 | ![skill 83](../gfx/skills/skill_83.png)   | grenade flash
![icon 82](../gfx/skills-equip/82-grenade.png)               |  82 | ![skill 82](../gfx/skills/skill_82.png)   | grenade
![icon 84](../gfx/skills-equip/84-grenade_gaz.png)           |  84 | ![skill 84](../gfx/skills/skill_84.png)   | grenade gaz
![icon 85](../gfx/skills-equip/85-grenade_clown.png)         |  85 | ![skill 85](../gfx/skills/skill_85.png)   | grenade clown
![icon 86](../gfx/skills-equip/86-grenade_fragment.png)      |  86 | ![skill 86](../gfx/skills/skill_86.png)   | grenade fragment
![icon 87](../gfx/skills-equip/87-grenade_secousse.png)      |  87 | ![skill 87](../gfx/skills/skill_87.png)   | grenade secousse
![icon 88](../gfx/skills-equip/88-grenade_gluante.png)       |  88 | ![skill 88](../gfx/skills/skill_88.png)   | grenade gluante
![icon 89](../gfx/skills-equip/89-grenade_benie.png)         |  89 | ![skill 89](../gfx/skills/skill_89.png)   | grenade benie
![icon 90](../gfx/skills-equip/90-grenade_soin.png)          |  90 | ![skill 90](../gfx/skills/skill_90.png)   | grenade soin
![icon 91](../gfx/skills-equip/91-grenade_trou_noir.png)     |  91 | ![skill 91](../gfx/skills/skill_91.png)   | grenade trou noir
![icon 103](../gfx/skills-equip/103-porte_bonheur.png)       | 103 | ![skill 103](../gfx/skills/skill_103.png) | porte bonheur
![icon 129](../gfx/skills-equip/129-twinoid.png)             | 129 | ![skill 129](../gfx/skills/skill_129.png) | twinoid
![icon 68](../gfx/skills-equip/68_aimant_de_combat.png)      |  68 | ![skill 68](../gfx/skills/skill_68.png)   | aimant de combat
![icon 100](../gfx/skills-equip/100-gilet_pare_balles.png)   | 100 | ![skill 100](../gfx/skills/skill_100.png) | gilet pare balles
![icon 54](../gfx/skills-equip/54-debardeur_Bruce.png)       |  54 | ![skill 54](../gfx/skills/skill_54.png)   | debardeur Bruce
![icon 101](../gfx/skills-equip/101-visee_laser.png)         | 101 | ![skill 101](../gfx/skills/skill_101.png) | visee laser
![icon 109](../gfx/skills-equip/109-radio.png)               | 109 | ![skill 109](../gfx/skills/skill_109.png) | radio
![icon 125](../gfx/skills-equip/125-talky_walky.png)         | 125 | ![skill 125](../gfx/skills/skill_125.png) | talky walky
![icon 51](../gfx/skills-equip/51-bipied.png)                |  51 | ![skill 51](../gfx/skills/skill_51.png)   | bipied
![icon 94](../gfx/skills-equip/94-detecteur_de_chaleur.png)  |  94 | ![skill 94](../gfx/skills/skill_94.png)   | detecteur de chaleur
![icon 118](../gfx/skills-equip/118-cagoule_amidonee.png)    | 118 | ![skill 118](../gfx/skills/skill_118.png) | cagoule amidonee
![icon 133](../gfx/skills-equip/133-baton_vaudou.png)        | 133 | ![skill 133](../gfx/skills/skill_133.png) | baton vaudou
![icon 50](../gfx/skills-equip/50-jumelles.png)              |  50 | ![skill 50](../gfx/skills/skill_50.png)   | jumelles
![icon 48](../gfx/skills-equip/48-extension_de_canon.png)    |  48 | ![skill 48](../gfx/skills/skill_48.png)   | extension de canon
![icon 59](../gfx/skills-equip/59-balle_percante.png)        |  59 | ![skill 59](../gfx/skills/skill_59.png)   | balle percante
![icon 71](../gfx/skills-equip/71-compensateur_de_recul.png) |  71 | ![skill 71](../gfx/skills/skill_71.png)   | compensateur de recul