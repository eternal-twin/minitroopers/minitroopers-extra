# Statistiques

### Crédits

Les crédits sont la monnaie virtuelle de MiniTroopers. Ils peuvent être utilisés pour acheter ou mettre à niveau vos soldats et débloquer des missions. On obtient des crédits pour un nombre limité d'activités.

#### Gains

- Victoires dans les batailles : `+2`
- Défaites dans les batailles : `+1`
- Victoires dans les missions : `+4`
- Victoires en raids, par phase : `+4`
- Recrutement :
   - 1<sup>er</sup> : `+100`
   - 2<sup>e</sup> ~ 4<sup>e</sup> : `+50`
   - 5<sup>e</sup> ~ 14<sup>e</sup> : `+10`
   - 15<sup>e</sup> ~ 34<sup>e</sup> : `+5`
   - 35<sup>e</sup> ~ ∞ : `0`

#### Coûts

##### Unités supplémentaires
- 2<sup>e</sup> : `-8`
- 3<sup>e</sup> : `-24`
- 4<sup>e</sup> : `-60`
- 5<sup>e</sup> : `-120`
- 6<sup>e</sup> : `-200`
- 7<sup>e</sup> : `-340`
- 8<sup>e</sup> : `-500`
- 9<sup>e</sup> : `-720`
- 10<sup>e</sup> : `-1000`
- 11<sup>e</sup> : `-1300`
- 12<sup>e</sup> : `-2000`

##### Amélioration (levelup)

`Cout = floor( sqrt( level ^ 5) )`

Pour améliorer une unité level 9 au level 11 :

`Cout = floor( sqrt( 9 ^ 5) ) = 316`

<details><summary>Relevés</summary>

Level | Coût | (+) | Ratio | Cumul
:-:|--:|--:|--:|--:
 1 | 0 |   |  | 0
 2 | 1 | 1 | 0.0% | 1
 3 | 5 | 4 | 500.0% | 6
 4 | 15 | 10 | 300.0% | 21
 5 | 32 | 17 | 213.3% | 53
 6 | 55 | 23 | 171.9% | 108
 7 | 88 | 33 | 160.0% | 196
 8 | 129 | 41 | 146.6% | 325
 9 | 181 | 52 | 140.3% | 506
 10 | 243 | 62 | 134.3% | 749
 11 | 316 | 73 | 130.0% | 1065
 12 | 401 | 85 | 126.9% | 1466
 13 | 498 | 97 | 124.2% | 1964
 14 | 609 | 111 | 122.3% | 2573
 15 | 733 | 124 | 120.4% | 3306
 16 | 871 | 138 | 118.8% | 4177
 17 | 1024 | 153 | 117.6% | 5201
 18 | 1191 | 167 | 116.3% | 6392
 19 | 1374 | 183 | 115.4% | 7766
 20 | 1573 | 199 | 114.5% | 9339
 21 | 1788 | 215 | 113.7% | 11127
 22 | 2020 | 232 | 113.0% | 13147
 23 | 2270 | 250 | 112.4% | 15417
 24 | 2536 | 266 | 111.7% | 17953
 25 | 2821 | 285 | 111.2% | 20774
 26 | 3125 | 304 | 110.8% | 23899
 27 | 3446 | 321 | 110.3% | 27345
 28 | 3787 | 341 | 109.9% | 31132
 29 | 4148 | 361 | 109.5% | 35280
 30 | 4528 | 380 | 109.2% | 39808
 31 | 4929 | 401 | 108.8% | 44737
 32 | 5350 | 421 | 108.5% | 50087
 33 | 5792 | 442 | 108.3% | 55879
 34 | 6255 | 463 | 108.0% | 62134
 35 | 6740 | 485 | 107.7% | 68874
 36 | 7247 | 507 | 107.5% | 76121
 37 | 7776 | 529 | 107.3% | 83897
 38 | 8327 | 551 | 107.1% | 92224
 39 | 8901 | 574 | 106.9% | 101125
 40 | 9498 | 597 | 106.7% | 110623
 41 | 10119 | 621 | 106.5% | 120742
 42 | 10763 | 644 | 106.4% | 131505
 43 | 11432 | 669 | 106.2% | 142937
 44 | 12124 | 692 | 106.0% | 155061
 45 | 12841 | 717 | 105.9% | 167902
 46 | 13584 | 743 | 105.8% | 181486
 47 | 14351 | 767 | 105.6% | 195837
</details>

### Puissance

La puissance est un paramètre statistique représentant la force combinée des Troopers d'une armée. Pour cette raison, les seuls moyens de l'augmenter sont d'acheter ou d'améliorer des soldats. Il n'est pas l'obtenir d'une autre source. En plus de cela, le pouvoir n'est utilisé pour rien, à part l'évaluation des cotes lors des combats de sélection et les classements.

La puissance est essentiellement calculée avec la formule suivante :

`Power = 4*T+L`

- **T :** nombre de soldats
- **L :** somme des niveaux de chaque soldats

