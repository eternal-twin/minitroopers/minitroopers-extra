# Armées notables

Note de liens utiles à fins de tests et d'informations. De préférence, n'ajouter que des armlées non-protégées par mot de passe.

### Top-10 des recrues des factions

 `#` | Couleur | ref CSS | Couleur (hex) | Couleur (dec)
:-:|---|:-:|:-:|--:
0 | Rouge  | .faction0 | `#E87D5F`  | `15236447`
1 | Orange | .faction1 | `#FFAA27`  | `16755239`
2 | Jaune  | .faction2 | `#FFDB7F`  | `16767871`
3 | Vert   | .faction3 | `#96B732`  | `9877298`
4 | Bleu   | .faction4 | `#9EADEF`  | `10399215`
5 | Violet | .faction5 | `#A55DC6`  | `10837446`

##### Top-10 FR

- Rouge : [L'armée de Sang-surs](http://deepnight.minitroopers.fr/ranks)
- Orange : [L'armée du Soleil rugissant](http://hiko.minitroopers.fr/ranks)
- Jaune : [L'armée du Gecko nevrosé](http://dalek.minitroopers.fr/ranks)
- Vert : [L'armée Enchantée des torses velus](http://eole.minitroopers.fr/ranks)
- Bleu : [L'armée de Fol-ecume](http://warp.minitroopers.fr/ranks)
- Violet : [L'armée du Groin celeste](http://whitetigle.minitroopers.fr/ranks)

##### Top-10 EN

- Rouge : [The Exploding Carrots](http://centerlink.minitroopers.com/ranks)
- Orange : [The Party-loving Hermits](http://jrorangee.minitroopers.com/ranks)
- Jaune : [The OK Lumberjacks](http://overlap.minitroopers.com/ranks)
- Vert : [The Dalai Farmers](http://ac201.minitroopers.com/ranks)
- Bleu : [The Order of the Sainted Trousers](http://bumbaclot.minitroopers.com/ranks)
- Violet : [The Wretched Individuals](http://xteen.minitroopers.com/ranks)

##### Top-10 ES

- Rouge : [El Ejército de Sangre fría](http://sombrerodepaja.minitroopers.es/ranks)
- Orange : [El Ejército del Sol creciente](http://la-mole.minitroopers.es/ranks) *Facción de la Vieja Guardia*
- Jaune : [El Ejército del Gran Imperio](http://miguel3.minitroopers.es/ranks) *Facción del soldado desconocido*
- Vert : [El Ejército de Hombres peludos](http://miguel02.minitroopers.es/ranks) *Facción de corazones heridos*
- Bleu : [El Ejército del Corazón salvaje](http://ejercito-6996.minitroopers.es/ranks) *Facción de hombres sin ley*
- Violet : [El Ejército del Ceño fruncido](http://miguel14.minitroopers.es/ranks) *Facción de balas perdidas*

### Grand nombre de recrues

Liens | effectifs 
---|:-:
<http://vache-rose.minitroopers.fr/hq> | 28655
<http://mathys-lacoste.minitroopers.fr/hq> | 528
<http://homerscarso.minitroopers.com/hq> | 514
<http://skavengers.minitroopers.com/hq> | 505
<http://taptoula.minitroopers.fr/hq> | 435
<http://alphstroopers.minitroopers.com/hq> | 394
<http://sde1.minitroopers.fr/hq> | 342
<http://lorenjupanu.minitroopers.com/hq> | 287
<http://loppermann.minitroopers.com/hq> | 130
<http://cinqaxes.minitroopers.fr/hq> | 123
<http://cd0.minitroopers.fr/hq> | 101
<http://qw0.minitroopers.fr/hq> | 89
<http://p0p1p2.minitroopers.fr/hq> | 87
<http://aze0.minitroopers.fr/hq> | 85
<http://quickbaby.minitroopers.com/hq> | 84
<http://r0t1.minitroopers.fr/hq> | 80
<http://oxymort.minitroopers.fr/hq> | 75
<http://0mat2.minitroopers.fr/hq> | 67
<http://n0n1.minitroopers.fr/hq> | 64
<http://sde14.minitroopers.fr/hq> | 64
<http://h0h.minitroopers.fr/hq> | 62
<http://frisson-2.minitroopers.fr/hq> | 60
<http://frisson-2.minitroopers.fr/hq> | 60
<http://ap011.minitroopers.fr/hq> | 57

### blindée de thunes (pour tester le levelup)

- <http://sies3.minitroopers.fr/hq>

### Unités de haut niveau

- lvl 37 : <http://r0t1.minitroopers.fr/t/0>

### Catalogues

Corpus d'unités pouvant servir pour analyses.

#### Unités Frénétiques

Journal des arbres de choix pour des unités suposées avoir *Frénétique* comme Skil de départ [source](https://minitroopers.fandom.com/wiki/Skill_Tree_for_Frenetic_Troopers)

- <http://helmknopf.minitroopers.com/t/0>
- <http://bobby-car.minitroopers.com/t/0>
- <http://freneticpistol.minitroopers.com/t/0>
- <http://freneticshotgun.minitroopers.com/t/0>
- <http://crime003.minitroopers.com/t/0>
- <http://fren1.minitroopers.com/t/0>
- <http://rszhrsht876.minitroopers.com/t/0>
- <http://dasgfadsg235.minitroopers.com/t/0>
- <http://dsafag1234.minitroopers.com/t/0>

#### Tank léger (Light tank)

- <http://geff.minitroopers.com/t/3>
- <http://gallade-lethalit.minitroopers.com/t/0>

#### Tank lourd

- <http://p0p1p2.minitroopers.fr/t/11>

#### Double Pistolet (Dual Pistol)

- <http://vieuxprout.minitroopers.fr/t/0>

#### Grenade clown

- <http://alphstroopers.minitroopers.com/t/1>
- <http://alphstroopers.minitroopers.com/t/7>

### Recherche d'une armée sans MDP

- FR : `site:http://minitroopers.fr/hq "Recruter un trooper"`
- EN : `site:http://minitroopers.com/hq "Recruit a trooper"`
