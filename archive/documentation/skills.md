Groupe | icon | id | obfu | deobfu | Nom (FR) | Nom (EN) | CSS x | CSS y
---|:-:|--:|---|---|---|---|--:|--:
Spécialisations | ![Skill n°0](../gfx/skills/skill_0.png)     |   0 | `)\\9h\x01`         | `SOLDAT`           | Soldat                | Soldier                  |    0 |    0
Spécialisations | ![Skill n°1](../gfx/skills/skill_1.png)     |   1 | `\x06x\x06@\x03`    | `MEDIC`            | Médecin               | Doctor                   |  -30 |    0
Spécialisations | ![Skill n°2](../gfx/skills/skill_2.png)     |   2 | `\x0bA!&`           | `PILOT`            | Pilote                | Pilot                    |  -60 |    0
Spécialisations | ![Skill n°3](../gfx/skills/skill_3.png)     |   3 | `o\f#7\x03`         | `ARTIFICER`        | Artificier            | Munitions                |  -90 |    0
Spécialisations | ![Skill n°4](../gfx/skills/skill_4.png)     |   4 | `YIby\x03`          | `SCOUT`            | Scout                 | Scout                    | -120 |    0
Spécialisations | ![Skill n°5](../gfx/skills/skill_5.png)     |   5 | `o(u\x01`           | `SPY`              | Espion                | Spy                      | -150 |    0
Spécialisations | ![Skill n°6](../gfx/skills/skill_6.png)     |   6 | `\x1fj\x180\x01`    |                    | Officier COM          | Comms. Officier          | -180 |    0
Spécialisations | ![Skill n°7](../gfx/skills/skill_7.png)     |   7 | `)H-u\x02`          | `SABOTEUR`         | Saboteur              | Saboteur                 | -210 |    0
Armes           | ![Skill n°8](../gfx/skills/skill_8.png)     |   8 | `(\x149N\x01`       | `PISTOL`           | Pistolet              | Pistol                   | -240 |    0
Armes           | ![Skill n°9](../gfx/skills/skill_9.png)     |   9 | `^\td8\x01`         | `REVOLVER`         | Revolver              | Revolver                 | -270 |    0
Armes           | ![Skill n°10](../gfx/skills/skill_10.png)   |  10 | `,\x0eq\x13\x03`    | `DESERT_EAGLE`     | Desert Eagle          | Desert Eagle             | -300 |    0
Armes           | ![Skill n°11](../gfx/skills/skill_11.png)   |  11 | `,!\x03q\x01`       | `BERETTA`          | Beretta               | Beretta                  | -330 |    0
Armes           | ![Skill n°12](../gfx/skills/skill_12.png)   |  12 | `\x13\x07Sc\x02`    |                    | Double pistolet       | Dual Pistols             | -360 |    0
Armes           | ![Skill n°13](../gfx/skills/skill_13.png)   |  13 | `HaHO`              | `SHOTGUN`          | Fusil                 | Shotgun                  | -390 |    0
Armes           | ![Skill n°14](../gfx/skills/skill_14.png)   |  14 | `J9I8\x02`          |                    | Fusil semi-auto       | Semi-Auto Shotgun        | -420 |    0
Armes           | ![Skill n°15](../gfx/skills/skill_15.png)   |  15 | `fm~\x01\x02`       |                    | Fusil double          | Double-Barrelled Shotgun | -450 |    0
Armes           | ![Skill n°16](../gfx/skills/skill_16.png)   |  16 | `\x18\tZX\x03`      |                    | Fusil à pompe         | Pump Action Shotgun      | -480 |    0
Armes           | ![Skill n°17](../gfx/skills/skill_17.png)   |  17 | `4\x05v\x1a\x01`    |                    | Fusil à grenailles    | Scattergun               | -510 |    0
Armes           | ![Skill n°18](../gfx/skills/skill_18.png)   |  18 | `\x03\x1cz-\x03`    |                    | Fusil d'assaut        | Assault Rifle            | -540 |    0
Armes           | ![Skill n°19](../gfx/skills/skill_19.png)   |  19 | `\x05\x15\x07\x01`  | `M16`              | M16                   | M16                      | -570 |    0
Armes           | ![Skill n°20](../gfx/skills/skill_20.png)   |  20 | `e\x140\t\x01`      | `AK47`             | AK47                  | AK47                     |    0 |  -30
Armes           | ![Skill n°21](../gfx/skills/skill_21.png)   |  21 | `f\x10\x02;\x01`    |                    | FAMAS                 | FAMAS                    |  -30 |  -30
Armes           | ![Skill n°22](../gfx/skills/skill_22.png)   |  22 | `\nJ&\x01`       | `THOMPSON`         | Thompson              | Thompson                 |  -60 |  -30
Armes           | ![Skill n°23](../gfx/skills/skill_23.png)   |  23 | `\x14\x0e{\x01`     | `UMP`              | UMP                   | UMP                      |  -90 |  -30
Armes           | ![Skill n°24](../gfx/skills/skill_24.png)   |  24 | `#\x18\nr\x01`      | `BAZOOKA_M1`       | Bazooka M1            | Bazooka M1               | -120 |  -30
Armes           | ![Skill n°25](../gfx/skills/skill_25.png)   |  25 | `;8TL\x02`          | `ROCKET_LAUNCHER`  | Lance-Roquettes       | Rocket Launcher          | -150 |  -30
Armes           | ![Skill n°26](../gfx/skills/skill_26.png)   |  26 | `*5x[\x02`          | `BAZOOKA_M25`      | Bazooka M25           | Bazooka M25              | -180 |  -30
Armes           | ![Skill n°27](../gfx/skills/skill_27.png)   |  27 | `\x1a\x17tQ`        |                    | Tube Infernal         | Infernal Tube            | -210 |  -30
Armes           | ![Skill n°28](../gfx/skills/skill_28.png)   |  28 | `ryG\x06`           |                    | Comanche Auto         | Comanche Auto            | -240 |  -30
Armes           | ![Skill n°29](../gfx/skills/skill_29.png)   |  29 | `c\x054k\x02`       | `MACHINE_GUN`      | Mitrailleuse Lourde   | Heavy machine gun        | -270 |  -30
Armes           | ![Skill n°30](../gfx/skills/skill_30.png)   |  30 | `bA\n-`             | `GATLING_GUN`      | Sulfateuse            | Gatling Gun              | -300 |  -30
Armes           | ![Skill n°31](../gfx/skills/skill_31.png)   |  31 | `\x15;Ax\x01`       | `MINIGUN`          | Minigun               | Minigun                  | -330 |  -30
Armes           | ![Skill n°32](../gfx/skills/skill_32.png)   |  32 | `X\x1fU`         | `SNIPER`           | Sniper                | Sniper                   | -360 |  -30
Armes           | ![Skill n°33](../gfx/skills/skill_33.png)   |  33 | `yLzf\x02`          | `MOS_TECK`         | MOS-TECK              | MOS-TECK                 | -390 |  -30
Armes           | ![Skill n°34](../gfx/skills/skill_34.png)   |  34 | ` \x1b\b7`          |                    | Lizaro Jungle         | Lizaro Jungle            | -420 |  -30
Armes           | ![Skill n°35](../gfx/skills/skill_35.png)   |  35 | `ns3e`              | `SPARROW_HAWK`     | Épervier              | SparrowHawk              | -450 |  -30
Armes           | ![Skill n°36](../gfx/skills/skill_36.png)   |  36 | `\x18\x0f; \x02`    | `CK_MAGELLAN`      | CK-Magellan           | CK-Magellan              | -480 |  -30
Armes           | ![Skill n°37](../gfx/skills/skill_37.png)   |  37 | `hp\x01A`           | `KNIFE`            | Couteau               | Knife                    | -510 |  -30
Véhicules       | ![Skill n°38](../gfx/skills/skill_38.png)   |  38 | `#wr_\x03`          |                    | Tank léger            | Light tank               | -540 |  -30
Véhicules       | ![Skill n°39](../gfx/skills/skill_39.png)   |  39 | `Q$\\\x15\x02`      |                    | Moto                  | Motorcycle               | -570 |  -30
Véhicules       | ![Skill n°40](../gfx/skills/skill_40.png)   |  40 | `\x134j\x10\x02`    |                    | Avion de Chasse       | Fighter Jet              |    0 |  -60
Véhicules       | ![Skill n°41](../gfx/skills/skill_41.png)   |  41 | `g\x1fn\x18\x03`    |                    | Hélicoptère           | Helicopter               |  -30 |  -60
Véhicules       | ![Skill n°42](../gfx/skills/skill_42.png)   |  42 | `K'\x05i\x01`       |                    | Tank Lourd            | Heavy Tank               |  -60 |  -60
Compétences     | ![Skill n°43](../gfx/skills/skill_43.png)   |  43 | `b\x10z5\x02`       | `ADRENALINE`       | Adrénaline            | Adrenaline               |  -90 |  -60
Compétences     | ![Skill n°44](../gfx/skills/skill_44.png)   |  44 | `\x1a,gm\x01`       |                    | Piqure d'amphet       | Amphetamine Shot         | -120 |  -60
Compétences     | ![Skill n°45](../gfx/skills/skill_45.png)   |  45 | `\x1fsZ4\x01`       | `ANATOMY`          | Anatomie              | Anatomy                  | -150 |  -60
Compétences     | ![Skill n°46](../gfx/skills/skill_46.png)   |  46 | `iq#~\x01`          |                    | Sac à dos             | Rucksack                 | -180 |  -60
Compétences     | ![Skill n°47](../gfx/skills/skill_47.png)   |  47 | `5 11`              | `BAIT`             | Appât                 | Bait                     | -210 |  -60
Compétences     | ![Skill n°48](../gfx/skills/skill_48.png)   |  48 | `i77'\x03`          | `BARREL_EXTENSION` | Extension de canon    | Barrel Extension         | -240 |  -60
Compétences     | ![Skill n°49](../gfx/skills/skill_49.png)   |  49 | `'j'A\x03`          | `BIG_CALVES`       | Gros Mollets          | Huge Calves              | -270 |  -60
Compétences     | ![Skill n°50](../gfx/skills/skill_50.png)   |  50 | `\x17tL_\x03`       | `BINOCULARS`       | Jumelles              | Binoculars               | -300 |  -60
Compétences     | ![Skill n°51](../gfx/skills/skill_51.png)   |  51 | ``` ``9]\x03```     | `BIPOD`            | Bipied                | Biped                    | -330 |  -60
Compétences     | ![Skill n°52](../gfx/skills/skill_52.png)   |  52 | `N&S`               |                    | Tir aveugle           | Blind Fury               | -360 |  -60
Compétences     | ![Skill n°53](../gfx/skills/skill_53.png)   |  53 | `\x19(l;\x01`       |                    | Vif                   | On point                 | -390 |  -60
Compétences     | ![Skill n°54](../gfx/skills/skill_54.png)   |  54 | `\x02I\x1aK`        |                    | Débardeur Bruce       | Wife Beater              | -420 |  -60
Compétences     | ![Skill n°55](../gfx/skills/skill_55.png)   |  55 | `~\x13ds`           | `BULL`             | Armoire à glace       | Brick S\*\*\*house       | -450 |  -60
Compétences     | ![Skill n°56](../gfx/skills/skill_56.png)   |  56 | `;H\x1fu\x01`       |                    | Balle Explosive       | Explosive Shells         | -480 |  -60
Compétences     | ![Skill n°57](../gfx/skills/skill_57.png)   |  57 | `\x16\rz\x01\x02`   |                    | Balle Hydroshock      | Hydroshock Shells        | -510 |  -60
Compétences     | ![Skill n°58](../gfx/skills/skill_58.png)   |  58 | `10;\x07`           |                    | Balle paralysante     | Paralysing Shells        | -540 |  -60
Compétences     | ![Skill n°59](../gfx/skills/skill_59.png)   |  59 | `#g'T\x03`          |                    | Balle perçante        | Armor-Piercing Shells    | -570 |  -60
Compétences     | ![Skill n°60](../gfx/skills/skill_60.png)   |  60 | `2\x04\x04/\x03`    |                    | Balle toxique         | Toxic shells             |    0 |  -90
Compétences     | ![Skill n°61](../gfx/skills/skill_61.png)   |  61 | `q,\x04;\x01`       | `CAMOUFLAGE`       | Camouflage            | Camouflage               |  -30 |  -90
Compétences     | ![Skill n°62](../gfx/skills/skill_62.png)   |  62 | `z\x1c\x1c'\x01`    |                    | Chargeur              | Loader                   |  -60 |  -90
Compétences     | ![Skill n°63](../gfx/skills/skill_63.png)   |  63 | `\x1eMUn`           | `CHARGE`           | Charge                | Charge                   |  -90 |  -90
Compétences     | ![Skill n°64](../gfx/skills/skill_64.png)   |  64 | `\x13\x12\x147`     |                    | Malin                 | Smart                    | -120 |  -90
Compétences     | ![Skill n°65](../gfx/skills/skill_65.png)   |  65 | `@@jC`              | `CLOUT`            | Grosse-beigne         | Fists of Fury            | -150 |  -90
Compétences     | ![Skill n°66](../gfx/skills/skill_66.png)   |  66 | `\b\\U#\x02`        |                    | Thermos de café       | Thermos of Coffee        | -180 |  -90
Compétences     | ![Skill n°67](../gfx/skills/skill_67.png)   |  67 | `h\x1fPw`           | `COLD_BLOOD`       | Sang Froid            | Cold Blooded             | -210 |  -90
Compétences     | ![Skill n°68](../gfx/skills/skill_68.png)   |  68 | `50\b\x1a`          |                    | Aimant de combat      | Scavenger                | -240 |  -90
Compétences     | ![Skill n°69](../gfx/skills/skill_69.png)   |  69 | `\x1e4()\x02`       | `COMMAND`          | Commandement          | Commander                | -270 |  -90
Compétences     | ![Skill n°70](../gfx/skills/skill_70.png)   |  70 | ```(\x1f0` ```      | `COMMANDO`         | Commando              | Commando                 | -300 |  -90
Compétences     | ![Skill n°71](../gfx/skills/skill_71.png)   |  71 | `\rswe\x03`         | `COMPENSATOR`      | Compensateur de recul | Compensator              | -330 |  -90
Compétences     | ![Skill n°72](../gfx/skills/skill_72.png)   |  72 | `D\x04\x13\x11\x03` | `COVER`            | A couvert!            | Take Cover!              | -360 |  -90
Compétences     | ![Skill n°73](../gfx/skills/skill_73.png)   |  73 | `i;j;`              |                    | Tir de couverture     | Covering Fire            | -390 |  -90
Compétences     | ![Skill n°74](../gfx/skills/skill_74.png)   |  74 | `+,v\x1e\x01`       |                    | Increvable            | Invincible               | -420 |  -90
Compétences     | ![Skill n°75](../gfx/skills/skill_75.png)   |  75 | `xtSb\x03`          |                    | Méfiant               | Suspicious               | -450 |  -90
Compétences     | ![Skill n°76](../gfx/skills/skill_76.png)   |  76 | `AGJ\b\x03`         | `DODGE`            | Esquive               | Dodger                   | -480 |  -90
Compétences     | ![Skill n°77](../gfx/skills/skill_77.png)   |  77 | `Y\x067 \x01`       | `FIRST_AID`        | Premiers-soins        | First Aid                | -510 |  -90
Compétences     | ![Skill n°78](../gfx/skills/skill_78.png)   |  78 | `\x1f^0\x0b\x01`    |                    | Débordement           | Out of Bounds            | -540 |  -90
Compétences     | ![Skill n°79](../gfx/skills/skill_79.png)   |  79 | `]{1\x1a\x03`       | `FRENZY`           | Frénésie              | Frenetic                 | -570 |  -90
Compétences     | ![Skill n°80](../gfx/skills/skill_80.png)   |  80 | `\x1f6((\x02`       | `FRIENDLY_FIRE`    | Tir Ami               | Friendly Fire            |    0 | -120
Compétences     | ![Skill n°81](../gfx/skills/skill_81.png)   |  81 | `&4y\f`             |                    | Armure complète       | Heavy Armor              |  -30 | -120
Compétences     | ![Skill n°82](../gfx/skills/skill_82.png)   |  82 | `\x03n\x16x\x03`    |                    | Grenade               | Grenade                  |  -60 | -120
Compétences     | ![Skill n°83](../gfx/skills/skill_83.png)   |  83 | `Wd\x017`           |                    | Grenade flash         | Flashbang                |  -90 | -120
Compétences     | ![Skill n°84](../gfx/skills/skill_84.png)   |  84 | `g\f\x0b\x01\x02`   |                    | Grenade Gaz           | Gas Grenade              | -120 | -120
Compétences     | ![Skill n°85](../gfx/skills/skill_85.png)   |  85 | `\x1b\x1fDW\x02`    |                    | Grenade Clown         | Clown Grenade            | -150 | -120
Compétences     | ![Skill n°86](../gfx/skills/skill_86.png)   |  86 | `\x01\x13\x14C`     |                    | Grenade Fragment      | Fragmentation grenade    | -180 | -120
Compétences     | ![Skill n°87](../gfx/skills/skill_87.png)   |  87 | `;\x10I\x01`        |                    | Grenade secousse      | Shock Grenade            | -210 | -120
Compétences     | ![Skill n°88](../gfx/skills/skill_88.png)   |  88 | `\x0bA\x1d(\x02`    |                    | Grenade gluante       | Glue Grenade             | -240 | -120
Compétences     | ![Skill n°89](../gfx/skills/skill_89.png)   |  89 | `C\t2\x06`          |                    | Grenade bénie         | Grenade benie            | -270 | -120
Compétences     | ![Skill n°90](../gfx/skills/skill_90.png)   |  90 | `\x02hD\x10\x01`    |                    | Grenade de soin       | Healing Grenade          | -300 | -120
Compétences     | ![Skill n°91](../gfx/skills/skill_91.png)   |  91 | `+++;\x02`          |                    | Grenade trou noir     | Black Hole Grenade       | -330 | -120
Compétences     | ![Skill n°92](../gfx/skills/skill_92.png)   |  92 | `v$\x07#`           | `HARD_BOILED`      | Dur-à-cuire           | Hard Boiled              | -360 | -120
Compétences     | ![Skill n°93](../gfx/skills/skill_93.png)   |  93 | `\x03cp1\x02`       |                    | Bourreau des cœurs    | Heartbreaker             | -390 | -120
Compétences     | ![Skill n°94](../gfx/skills/skill_94.png)   |  94 | `\x06\x12%N\x02`    |                    | Détecteur de chaleur  | Heat Sensor              | -420 | -120
Compétences     | ![Skill n°95](../gfx/skills/skill_95.png)   |  95 | `J_R,`              |                    | Poids lourd           | Heavyweight              | -450 | -120
Compétences     | ![Skill n°96](../gfx/skills/skill_96.png)   |  96 | `o\x01Z%\x01`       | `HURRY`            | Pressé                | Hurry                    | -480 | -120
Compétences     | ![Skill n°97](../gfx/skills/skill_97.png)   |  97 | `@K\x06\x1c\x01`    | `HYPERACTIVE`      | Hyperactif            | Hyperactive              | -510 | -120
Compétences     | ![Skill n°98](../gfx/skills/skill_98.png)   |  98 | `1Y%\'\x02`         | `INTERCEPTION`     | Interception          | Interception             | -540 | -120
Compétences     | ![Skill n°99](../gfx/skills/skill_99.png)   |  99 | `N\x19\x0fU\x02`    | `JUGGLER`          | Jongleur              | Juggler                  | -570 | -120
Compétences     | ![Skill n°100](../gfx/skills/skill_100.png) | 100 | `1vDS\x02`          |                    | Gilet pare-balles     | Bulletproof Vest         |    0 | -150
Compétences     | ![Skill n°101](../gfx/skills/skill_101.png) | 101 | `9\x02\x14-\x03`    |                    | Visée Laser           | Laser Sights             |  -30 | -150
Compétences     | ![Skill n°102](../gfx/skills/skill_102.png) | 102 | `;B=u\x02`          | `LAST_MOHICAN`     | Dernier Mohican       | Last Mohican             |  -60 | -150
Compétences     | ![Skill n°103](../gfx/skills/skill_103.png) | 103 | `h$qD`              | `LUCKY_CHARM`      | Porte Bonheur         | Lucky Charm              |  -90 | -150
Compétences     | ![Skill n°104](../gfx/skills/skill_104.png) | 104 | `iU1O`              |                    | Mitrailleur           | Tail Gunner              | -120 | -150
Compétences     | ![Skill n°105](../gfx/skills/skill_105.png) | 105 | `o%Ua\x02`          | `NIMBLE_FINGERS`   | Doigts-Agiles         | Nimble Fingers           | -150 | -150
Compétences     | ![Skill n°106](../gfx/skills/skill_106.png) | 106 | `Z\x17LR\x03`       | `NERVOUS`          | Nerveux               | Nervous                  | -180 | -150
Compétences     | ![Skill n°107](../gfx/skills/skill_107.png) | 107 | `K\n\x18)`          | `OCCUPATION`       | Occupation            | Occupation               | -210 | -150
Compétences     | ![Skill n°108](../gfx/skills/skill_108.png) | 108 | `\x05n*`         |                    | Roi de la pétanque    | King of Boules           | -240 | -150
Compétences     | ![Skill n°109](../gfx/skills/skill_109.png) | 109 | `);ed\x01`          | `RADIO`            | Radio                 | Radio                    | -270 | -150
Compétences     | ![Skill n°110](../gfx/skills/skill_110.png) | 110 | `_9?2\x01`          |                    | Attaque à revers      | Reverse Attack           | -300 | -150
Compétences     | ![Skill n°111](../gfx/skills/skill_111.png) | 111 | `c&^X`              | `RESCUER`          | Sauveteur             | Saviour                  | -330 | -150
Compétences     | ![Skill n°112](../gfx/skills/skill_112.png) | 112 | `3\tU;\x03`         | `RESILIENCE`       | Ressort               | Bounce Back              | -360 | -150
Compétences     | ![Skill n°113](../gfx/skills/skill_113.png) | 113 | `Bxa\f\x02`         |                    | Sans repos            | Restless                 | -390 | -150
Compétences     | ![Skill n°114](../gfx/skills/skill_114.png) | 114 | `8;\x0b\x0f`        | `RUSH`             | Ruée                  | Rush                     | -420 | -150
Compétences     | ![Skill n°115](../gfx/skills/skill_115.png) | 115 | `\x13HU\n\x02`      |                    | Œil de Lynx           | Eye of the Tiger         | -450 | -150
Compétences     | ![Skill n°116](../gfx/skills/skill_116.png) | 116 | `\x17r$i\x03`       |                    | Rancunier             | Unforgiving              | -480 | -150
Compétences     | ![Skill n°117](../gfx/skills/skill_117.png) | 117 | `\x10\nP\x1f\x01`   |                    | Sprinteur             | Sprinter                 | -510 | -150
Compétences     | ![Skill n°118](../gfx/skills/skill_118.png) | 118 | `y+\x1d+\x02`       |                    | Cagoule amidonnée     | Full Metal Balaclava     | -540 | -150
Compétences     | ![Skill n°119](../gfx/skills/skill_119.png) | 119 | `Qa%9\x02`          |                    | Forte poigne          | Death Grip               | -570 | -150
Compétences     | ![Skill n°120](../gfx/skills/skill_120.png) | 120 | `QnS\x07\x01`       |                    | Cascadeur             | Fall Guy                 |    0 | -180
Compétences     | ![Skill n°121](../gfx/skills/skill_121.png) | 121 | `!\x14S\t\x01`      | `STURDY`           | Robuste               | Sturdy                   |  -30 | -180
Compétences     | ![Skill n°122](../gfx/skills/skill_122.png) | 122 | `\x1f\f t\x01`      |                    | Coup de tatane        | Faceboot                 |  -60 | -180
Compétences     | ![Skill n°123](../gfx/skills/skill_123.png) | 123 | `(~K[`              | `SURVIVOR`         | Survivant             | Survivor                 |  -90 | -180
Compétences     | ![Skill n°124](../gfx/skills/skill_124.png) | 124 | ```t`BP\x02```      | *mot composé*      | Instinct de survie    | Survival Instinct        | -120 | -180
Compétences     | ![Skill n°125](../gfx/skills/skill_125.png) | 125 | `$__,\x01`          | `TALKY_WALKY`      | Talky-Walky           | Talky-Walky              | -150 | -180
Compétences     | ![Skill n°126](../gfx/skills/skill_126.png) | 126 | `\x07$8G\x02`       | `TRAMPLE`          | Piétinement           | Stamp                    | -180 | -180
Compétences     | ![Skill n°127](../gfx/skills/skill_127.png) | 127 | `0i1=`              | `TRIGGER_HAPPY`    | Gâchette facile       | Trigger Happy            | -210 | -180
Compétences     | ![Skill n°128](../gfx/skills/skill_128.png) | 128 | `\x15\x05\x19(\x02` | *mot composé*      | Roulé-boulé           | Tuck 'n' Roll            | -240 | -180
Compétences     | ![Skill n°129](../gfx/skills/skill_129.png) | 129 | `\x01\x07qj`        | `TWINOID`          | Twinoïde              | Twinoid                  | -270 | -180
Compétences     | ![Skill n°130](../gfx/skills/skill_130.png) | 130 | `=STS`              | `UNSHAKABLE`       | Inébranlable          | Unshakable               | -300 | -180
Compétences     | ![Skill n°131](../gfx/skills/skill_131.png) | 131 | `}\x10!)\x01`       | `VENDETTA`         | Vendetta              | Vendetta                 | -330 | -180
Compétences     | ![Skill n°132](../gfx/skills/skill_132.png) | 132 | `ygnz\x03`          |                    | Vicieux               | Vicious                  | -360 | -180
Compétences     | ![Skill n°133](../gfx/skills/skill_133.png) | 133 | `*\x135r\x03`       |                    | Bâton vaudou          | Voodoo Doll              | -390 | -180
Compétences     | ![Skill n°134](../gfx/skills/skill_134.png) | 134 | ` *gn\x03`          |                    | Bien organisé         | Battle-Ready             | -420 | -180
Compétences     | ![Skill n°135](../gfx/skills/skill_135.png) | 135 | `\x017CT\x02`       |                    | Geignard              | Crybaby                  | -450 | -180
Compétences     | ![Skill n°136](../gfx/skills/skill_136.png) | 136 | `li9)\x01`          | `WRESTLING`        | Lutte                 | Wrestler                 | -480 | -180
Compétences     | ![Skill n°137](../gfx/skills/skill_137.png) | 137 | `\x18DJL`           |                    | Coureur en zigzag     | Zigzag                   | -510 | -180
Compétences     | ![Skill n°138](../gfx/skills/skill_138.png) | 138 | `,\x02\bS\x01`      | `ENTHUSIAST`       | Enthousiaste          | Enthusiastic             | -540 | -180
Compétences     | ![Skill n°139](../gfx/skills/skill_139.png) | 139 | `*Xbz`              | `MARTYR`           | Martyre               | Martyr                   | -570 | -180
Exclusive (Clown) | aucune                                    | 140 | `x\x18k\x01`     |                    | Semelles de plomb     | Semelles de plomb        | x    | x