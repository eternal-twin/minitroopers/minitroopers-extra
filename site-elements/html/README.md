# Sauvegarde des sources HTML

A effectuer sur les 3 sites :

- minitroopers.fr
- minitroopers.com (EN)
- minitroopers.es
- ~~mintoopers.net~~ (down)

## Checklist FR

 Fait | url | type | Note 
---|---|---|---
- [x] | `/` | public | Page d'accueil
- [x] | `/add` | privé | Ajoute un trooper
- [x] | `/b/fview/262575101` | public | Scène de bataille (opposant choisi)
- [x] | `/b/opp` | privé | Choix entre différents opposants.
- [x] | `/b/view/bat1` | public | Scène de bataille (opposant proposé)
- [x] | `/b/view/miss1` | public | Mission
- [x] | `/b/view/raid` | public | Raid
- [x] | `/choice` | privé | Gérer mon armée
- [x] | `/endDay` | privé | Fin
- [x] | `/history` | privé | Historique
- [x] | `/hq` | public | Quartier Général
- [x] | `/levelup/0` | privé | Levelup
- [x] | `/protect` | public | Protéger mon armée
- [x] | `/ranks` | public | Hiérarchie
- [x] | `/recruit` | Modal, public | Recruter un trooper
- [x] | `/t/0` | privé | Mes Troopers, Fiche
- [x] | `/check?name=Warp` | public | Check (webservice)
- [x] | `/load?pos=0;count=16` | privée | Load (webservice)
- [x] | `/b/log?b=262576016` | public | (webservice)

## Checklist COM (EN)

 Fait | url | type | Note 
---|---|---|---
- [x] | `/` | public | Page d'accueil
- [x] | `/add` | privé | Ajoute un trooper
- [x] | `/b/fview/262575101` | public | Scène de bataille (opposant choisi)
- [x] | `/b/opp` | privé | Choix entre différents opposants.
- [x] | `/b/view/bat1` | public | Scène de bataille (opposant proposé)
- [x] | `/b/view/miss1` | public | Mission
- [x] | `/b/view/raid` | public | Raid
- [x] | `/choice` | privé | Gérer mon armée
- [x] | `/endDay` | privé | Fin
- [x] | `/history` | privé | Historique
- [x] | `/hq` | public | Quartier Général
- [x] | `/levelup/0` | privé | Levelup
- [x] | `/protect` | public | Protéger mon armée
- [x] | `/ranks` | public | Hiérarchie
- [x] | `/recruit` | Modal, public | Recruter un trooper
- [x] | `/t/0` | privé | Mes Troopers, Fiche

## Checklist ES

 Fait | url | type | Note 
---|---|---|---
- [ ] | `/` | public | Page d'accueil
- [ ] | `/add` | privé | Ajoute un trooper
- [ ] | `/b/fview/262575101` | public | Scène de bataille (opposant choisi)
- [ ] | `/b/opp` | privé | Choix entre différents opposants.
- [ ] | `/b/view/bat1` | public | Scène de bataille (opposant proposé)
- [ ] | `/b/view/miss1` | public | Mission
- [ ] | `/b/view/raid` | public | Raid
- [ ] | `/choice` | privé | Gérer mon armée
- [ ] | `/endDay` | privé | Fin
- [ ] | `/history` | privé | Historique
- [ ] | `/hq` | public | Quartier Général
- [ ] | `/levelup/0` | privé | Levelup
- [ ] | `/protect` | public | Protéger mon armée
- [ ] | `/ranks` | public | Hiérarchie
- [ ] | `/recruit` | Modal, public | Recruter un trooper
- [ ] | `/t/0` | privé | Mes Troopers, Fiche