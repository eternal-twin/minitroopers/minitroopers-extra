package §\x1ei[\x1d§.§/_\x01§
{
   import flash.utils.ByteArray;
   import flash.utils.Endian;
   import §pG?\x06\x03§.§\bRQ\f§;
   
   public class §u\x1a9;\x02§ extends §B4VV\x01§
   {
       
      
      public var §2\x01§:ByteArray;
      
      public function §u\x1a9;\x02§()
      {
         if(§\bRQ\f§.§U4V9\x03§)
         {
            return;
         }
         §2\x01§ = new ByteArray();
         §2\x01§.endian = Endian.LITTLE_ENDIAN;
      }
      
      override public function §3p\x03U\x02§(param1:int) : void
      {
         if(param1 < 0 || param1 >= 1073741824)
         {
            §\bRQ\f§.§\x07Bu#\x02§ = new Error();
            throw §5\x10xN§.§N?h\x18\x01§;
         }
         §2\x01§.writeInt(param1);
      }
      
      override public function oOQ9(param1:int) : void
      {
         if(param1 < 0 || param1 >= 65536)
         {
            §\bRQ\f§.§\x07Bu#\x02§ = new Error();
            throw §5\x10xN§.§N?h\x18\x01§;
         }
         §2\x01§.writeShort(param1);
      }
      
      override public function §AYi \x02§(param1:String) : void
      {
         §2\x01§.writeUTFBytes(param1);
      }
      
      override public function §\x12T\bF§(param1:int) : void
      {
         if(param1 < -128 || param1 >= 128)
         {
            §\bRQ\f§.§\x07Bu#\x02§ = new Error();
            throw §5\x10xN§.§N?h\x18\x01§;
         }
         §2\x01§.writeByte(param1);
      }
      
      override public function §E)E$\x01§(param1:int) : void
      {
         §2\x01§.writeInt(param1);
      }
      
      override public function §0M\x1bO§(param1:int) : void
      {
         if(param1 < -1073741824 || param1 >= 1073741824)
         {
            §\bRQ\f§.§\x07Bu#\x02§ = new Error();
            throw §5\x10xN§.§N?h\x18\x01§;
         }
         §2\x01§.writeInt(param1);
      }
      
      override public function §H\x05&P\x03§(param1:int) : void
      {
         if(param1 < -32768 || param1 >= 32768)
         {
            §\bRQ\f§.§\x07Bu#\x02§ = new Error();
            throw §5\x10xN§.§N?h\x18\x01§;
         }
         §2\x01§.writeShort(param1);
      }
      
      override public function §3 tA\x01§(param1:Number) : void
      {
         §2\x01§.writeFloat(param1);
      }
      
      override public function §\x1c%X+\x03§(param1:Number) : void
      {
         §2\x01§.writeDouble(param1);
      }
      
      override public function §V\x11go\x01§(param1:§\x17%1 \x01§, param2:int, param3:int) : int
      {
         if(param2 < 0 || param3 < 0 || param2 + param3 > param1.§61 \x0b\x01§)
         {
            §\bRQ\f§.§\x07Bu#\x02§ = new Error();
            throw §5\x10xN§.§dXB\x1e\x03§;
         }
         §2\x01§.writeBytes(param1.§2\x01§,param2,param3);
         return param3;
      }
      
      override public function §%tW?\x01§(param1:int) : void
      {
         §2\x01§.writeByte(param1);
      }
      
      override public function §EE0+\x03§(param1:Boolean) : Boolean
      {
         §(\x152j§ = param1;
         §2\x01§.endian = !!param1?Endian.BIG_ENDIAN:Endian.LITTLE_ENDIAN;
         return param1;
      }
      
      override public function §IPy4\x03§(param1:int) : void
      {
         if(param1 > 0)
         {
            §2\x01§[param1 - 1] = int(§2\x01§[param1 - 1]);
         }
      }
      
      public function §t0lO\x01§() : §\x17%1 \x01§
      {
         var _loc1_:ByteArray = §2\x01§;
         §2\x01§ = null;
         return new §\x17%1 \x01§(_loc1_.length,_loc1_);
      }
   }
}
