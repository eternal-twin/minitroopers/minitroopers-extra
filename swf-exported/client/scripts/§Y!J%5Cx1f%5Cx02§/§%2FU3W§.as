package §Y!J\x1f\x02§
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.FrameLabel;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.geom.Matrix;
   import flash.geom.Rectangle;
   import §pG?\x06\x03§.§\bRQ\f§;
   
   public class §/U3W§ extends MovieClip
   {
       
      
      public var §w2i\x01§:Object;
      
      public var §F;R\x12\x01§:Bitmap;
      
      public var §\t;`\r\x01§:Number;
      
      public var §Zw\x04\x03\x02§:Function;
      
      public var §&~\x03'§:Boolean;
      
      public var §)-Z\x19\x01§:Number;
      
      public var §A\x17/V§:Object;
      
      public function §/U3W§(param1:MovieClip = undefined, param2:Number = 1.0)
      {
         if(§\bRQ\f§.§U4V9\x03§)
         {
            return;
         }
         super();
         if(param1 != null)
         {
            §\x05EHd§(param1,param2);
         }
         §F;R\x12\x01§ = new Bitmap();
         addChild(§F;R\x12\x01§);
         §)-Z\x19\x01§ = 0;
         §\t;`\r\x01§ = 1;
         §&~\x03'§ = true;
      }
      
      public static function §w};\x1b§(param1:MovieClip, param2:Number = 1.0, param3:Boolean = true) : Object
      {
         var _loc9_:int = 0;
         var _loc10_:* = null as Rectangle;
         var _loc17_:int = 0;
         var _loc18_:* = null as BitmapData;
         var _loc20_:* = null as FrameLabel;
         var _loc4_:Number = param2;
         param1.scaleY = _loc4_;
         param1.scaleX = _loc4_;
         var _loc5_:Sprite = new Sprite();
         _loc5_.addChild(param1);
         var _loc6_:Rectangle = param1.getBounds(_loc5_);
         var _loc7_:int = 0;
         var _loc8_:int = param1.totalFrames;
         while(_loc7_ < _loc8_)
         {
            _loc7_++;
            _loc9_ = _loc7_;
            param1.gotoAndStop(_loc9_ + 1);
            _loc10_ = param1.getBounds(_loc5_);
            _loc6_.top = Number(Math.min(_loc6_.top,_loc10_.top));
            _loc6_.left = Number(Math.min(_loc6_.left,_loc10_.left));
            _loc6_.bottom = Number(Math.max(_loc6_.bottom,_loc10_.bottom));
            _loc6_.right = Number(Math.max(_loc6_.right,_loc10_.right));
         }
         _loc7_ = int(_loc6_.x);
         _loc8_ = int(_loc6_.y);
         _loc9_ = int(Math.ceil(_loc6_.width));
         var _loc11_:int = int(Math.ceil(_loc6_.height));
         var _loc12_:Array = [];
         var _loc13_:Matrix = new Matrix();
         var _loc14_:int = 0;
         var _loc15_:int = 0;
         var _loc16_:int = param1.totalFrames;
         while(_loc15_ < _loc16_)
         {
            _loc15_++;
            _loc17_ = _loc15_;
            param1.gotoAndStop(_loc17_ + 1);
            _loc10_ = param1.getBounds(_loc5_);
            _loc18_ = new BitmapData(_loc9_,_loc11_,true,0);
            _loc13_.identity();
            _loc13_.scale(param2,param2);
            _loc13_.translate(_loc10_.x - _loc7_ - _loc10_.x,_loc10_.y - _loc8_ - _loc10_.y);
            _loc18_.draw(param1,_loc13_);
            _loc12_.push({
               "UK\r\x01":_loc18_,
               "t\x14S6\x03":null,
               "e\'w\x1e":_loc10_
            });
         }
         _loc15_ = 0;
         var _loc19_:Array = param1.currentLabels;
         while(_loc15_ < int(_loc19_.length))
         {
            _loc20_ = _loc19_[_loc15_];
            _loc15_++;
            _loc12_[_loc20_.frame - 1].§t\x14S6\x03§ = _loc20_.name;
            if(_loc20_.name == "loop")
            {
               _loc14_ = _loc20_.frame - 1;
            }
         }
         return {
            "w2i\x01":_loc12_,
            "\x16x\x06\x14":_loc14_,
            "v\x01":_loc7_,
            "l\x01":_loc8_
         };
      }
      
      public function §&l\f[\x01§() : void
      {
         if(§\t;`\r\x01§ == 0)
         {
            return;
         }
         §)-Z\x19\x01§ = Number(§)-Z\x19\x01§ + §\t;`\r\x01§);
         var _loc1_:int = int(§)-Z\x19\x01§);
         if(_loc1_ == int(§w2i\x01§.§w2i\x01§.length))
         {
            if(§&~\x03'§)
            {
               §)-Z\x19\x01§ = int(§w2i\x01§.§\x16x\x06\x14§);
            }
            else
            {
               §\t;`\r\x01§ = 0;
               §)-Z\x19\x01§ = §)-Z\x19\x01§ - 1;
            }
            if(§Zw\x04\x03\x02§ != null)
            {
               §Zw\x04\x03\x02§();
            }
         }
         §T\x19W\x13\x02§();
      }
      
      public function §\x10fX\x07\x02§(param1:Object) : void
      {
         §w2i\x01§ = param1;
         §F;R\x12\x01§.x = int(§w2i\x01§.§v\x01§);
         §F;R\x12\x01§.y = int(§w2i\x01§.§l\x01§);
         §T\x19W\x13\x02§();
      }
      
      public function §\x05EHd§(param1:MovieClip, param2:Number) : void
      {
         §\x10fX\x07\x02§(§/U3W§.§w};\x1b§(param1,param2));
      }
      
      public function aGx2() : void
      {
      }
      
      public function §T\x19W\x13\x02§() : void
      {
         §A\x17/V§ = §w2i\x01§.§w2i\x01§[int(§)-Z\x19\x01§)];
         §F;R\x12\x01§.bitmapData = §A\x17/V§.§UK\r\x01§;
      }
   }
}
